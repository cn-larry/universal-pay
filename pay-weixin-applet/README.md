
![img_2.png](../images/img_2.png)

###1、前言

本小程序为微信小程序，介绍注册、开发、调试、上线等。
适用于准备开发或有一定小程序开发能力的人员。
不管是是window、mac还是其他什么电脑，只要你的电脑可以安装"微信开发者工具"就行。

###2、小程序注册

[微信官方注册地址](https://mp.weixin.qq.com/wxopen/waregister?action=step1&token=&lang=zh_CN)

注册说明：需要您一个没有注册过微信相关应用的邮箱和一个微信实名认证了的微信号
>友情提醒：
>>一个QQ邮箱可以注册3个小程序哦，不知道的赶快私聊我哟

###3、小程序开发环境搭建
下载[微信开发者工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)

选择适合您电脑的版本点击下载即可

###4、小程序代码导入
将[pay-weixin-applet](https://gitee.com/llhf/universal-pay/tree/master/pay-weixin-applet)
下载下来，使用您的"微信开发者工具"打开项目即可

###5、小程序代码配置修改

    a、小程序APPID修改
![img_1.png](../images/img_1.png)

    b、小程序后台接口地址修改
（如果本地调试，可不用修改投产发布记得修改哦）
![img.png](../images/img.png)

###6、小程序本地调试

    a、生成订单号
您可以使用swagger请求你本地的后台生成订单号：

http://localhost/swagger-ui/index.html#/收款交易相关Controller/appletCreateUsingPOST

或者你使用curl命令请求您本地的后台服务生成订单号：

curl -X POST "http://localhost:80/qrCodePay/createAppletUrl?merId=100&payAmt=423&payType=WEIXIN&payUserId=1122212112112" -H "accept: */*" -d ""

    b、查看后台日志输出的订单号：
![img_3.png](../images/img_3.png)

    c、小程序自定义编译

编辑文件"project.private.config.json"，miniprogram.list列表内容将是您的自定义编译参数；

query里面的payNo值替换成您生成的订单号即可

###7、小程序发布

小程序调试没问题后即可提交您的小程序代码，

登录[微信小程序后台](https://mp.weixin.qq.com)提交小程序审核发布即可

>温馨提示
>> 建议早上9点左右提交审核；不建议下午17点之后提交代码审核，由于排队机制，和审核流程，您的审核速度会更慢
>> （审核基本上算是"白盒测试"，您的功能必须都正常、且符合经营类目，建议删除无关的页面代码或隐藏相关无用的页面代码）


###8、小程序体验

    a、小程序页面跳转体验码（使用微信扫一扫）
![img_4.png](../images/img_4.png)

可以通过小程序二维码扫码进入指定页面，并且可以携带参数加载。

    b、小程序页面跳转体验链接（可在任意移动APP里面打开）

由于个人主体的小程序无法申请 微信小程序URL Link，
我在这无法给您体验。

功夫不负有心人，我找到一个卖海鲜的小程序。
里面有个商品分享的功能，可以获取带参数的 微信小程序URL Link。

下面的是小程序二维码：
![img_5.png](../images/img_5.png)

进入小程序商品详情页面，获得小程序页面参数链接：https://wxaurl.cn/SzZLXJuJivq
您可以复制该链接到QQ、支付宝、今日头条、抖音、快手等非微信APP里面打开。

如果您可以在非微信APP里面打开上面的链接，说明您可以实现在非微信APP里面进行微信支付。

恭喜您成功了！



###9、致谢

感谢您对我这个小程序的支持（点个小星星支持一下哟 : ）。

如果您有什么建议或意见，欢迎留言，我将会及时回复您。

如果您需要进行技术交流，请+QQ群：299838785
