var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mer_name: '',
    order_no: '',
    pay_time: '',
    pay_amt: '',
    order_describe: '',
    mer_order_no: '',
    order_body: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getOpenid();
    if(options.payNo){
      this.setData({
        order_no: options.payNo
      })
      this.getOrder();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // },

  //------获取微信code---------
  async getOpenid() {
    var that = this;
    // console.log(app.globalData.wxOpenid)
    if (app.globalData.wxOpenid != '-') {
      return;
    }
    //------执行Login---------
    wx.login({
      success: res => {
        console.log('code：', res.code);
        wx.request({
          url: app.globalData.sysUrl + 'qrCode/weixin/getOpenid',
          data: {
            code: res.code
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          success: res => {
            console.log("请求应答: ", res.data);
            if (res.data.code == '0') {
              app.globalData.wxOpenid = res.data.msg;
              this.getOrder();
            } else {
              wx.showToast({
                title: res.data.msg,
                icon: 'none',
              })
              that.getOpenid();
            }
          },
          fail: function (err) {
            console.log("获取失败: ", err);
            that.getOpenid();
          }
        })
      }
    });
  },

  //------ 查询订单信息 ---------
  getOrder() {
    var that = this;
    console.log("请求应答: ", app.globalData.wxOpenid);
    if (app.globalData.wxOpenid == '-') {
      return;
    }
    //------ 查询订单信息 ---------
    wx.request({
      url: app.globalData.sysUrl + 'qrCodePay/appletCreate',
      data: {
        payNo: that.data.order_no
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      success: res => {
        console.log("请求应答: ", res.data);
        if (res.data.code == '0') {
          that.setData({
            mer_name: res.data.data.payMer.merName,
            pay_amt: res.data.data.payAmt,
            prepay_id: res.data.data.prepayId,
            order_no: res.data.data.payNo
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
          })
          this.setData({
            pay_disabled: 'disabled'
          })
        }
      },
      fail: function (err) {
        console.log("获取失败: ", err);
        that.getOpenid();
        this.setData({
          pay_disabled: 'disabled'
        })
      }
    })
  },


  pay() {
    this.setData({
      submitState: 'loading'
    })
    var that = this;
    console.log(that.data.pay_amt)
    if(that.verify_pay_amt()){
      return;
    }
    wx.requestPayment({
      "timeStamp": this.data.prepay_id.timeStamp,
      "nonceStr": this.data.prepay_id.nonceStr,
      "package": this.data.prepay_id.package,
      "signType": this.data.prepay_id.signType,
      "paySign": this.data.prepay_id.paySign,
      "success":function(res){
        // console.log("支付成功");
        that.navigateTosuccess();
      },
      "fail":function(res){
        // console.log("支付失败");
        that.navigateTosuccess();
      },
      "complete":function(res){
        // 根据业务是否一定要跳转到成功页面
        that.navigateTosuccess();
      }
    })
  },

  verify_pay_amt(){
    if(this.data.pay_amt<1){
      wx.showToast({
        title: "付款金额不能为空",
        icon: 'none',
      })
      this.setData({
        pay_disabled: 'disabled',
        submitState: ''
      })
      return true;
    }else
      return false;
  },

  navigateToFail(){
    wx.navigateTo({ url: '../payfail/payfail?pay_amt=' + this.data.pay_amt + '&mer_name=' + that.data.mer_name })
  },
  navigateTosuccess(){
    wx.navigateTo({ url: '../paysuccess/paysuccess?order_no=' + this.data.order_no})
  },

})