Page({

  /**
   * 页面的初始数据
   */
  data: {
    mer_name: '',
    order_no: '',
    pay_time: '',
    pay_amt: '',
    order_describe: '',
    mer_order_no: '',
    order_body: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      mer_name: options.mer_name,
      order_no: options.order_no,
      pay_time: options.pay_time,
      pay_amt: options.pay_amt,
      order_describe: options.order_describe,
      mer_order_no: options.mer_order_no,
      order_body: options.order_body
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

})