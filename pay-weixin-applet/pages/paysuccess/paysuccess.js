var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mer_name: '',
    order_no: '',
    pay_time: '',
    order_describe: '',
    mer_order_no: '',
    order_body: '',
    pay_amt:'',
    pay_status:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options){
      this.setData({
        order_no: options.order_no
      })
      this.querOrder();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {
  // },


  navigateToFail(){
    wx.navigateTo({ url: '../payfail/payfail?pay_amt=' + this.data.pay_amt + '&mer_name=' + this.data.mer_name })
  },

  // 交易查询
  querOrder() {
    var that = this;
    wx.request({
      url: app.globalData.sysUrl + 'qrCodePay/query',
      data: {
        payNo: that.data.order_no,
        payType: 'WEIXIN'
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      success: res => {
        console.log("请求应答: ", res.data);
        if (res.data.code == '0') {
          that.setData({
            pay_amt: res.data.data.payAmt,
            order_no: res.data.data.payNo,
            mer_name: res.data.data.payMer.merName,
            pay_status: res.data.data.payMer.payStatus,
          })
          // 判断是否支付成功
          if('SUCCESS'!=that.data.pay_status)
            that.navigateToFail();
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
          })
          that.navigateToFail();
        }
      },
      fail: function (err) {
        console.log("获取失败: ", err);
        that.getOpenid();
        this.setData({
          pay_disabled: 'disabled'
        })
      }
    })
  },

  
})