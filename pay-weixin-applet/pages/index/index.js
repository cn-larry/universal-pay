const app = getApp()

Page({
  data: {
    like: 'likefill',
    contentList: [],
    avatarUrl: '',
    list: [{
      title: '嫣红',
      name: 'red',
      color: '#e54d42'
    },
    {
      title: '桔橙',
      name: 'orange',
      color: '#f37b1d'
    },
    {
      title: '明黄',
      name: 'yellow',
      color: '#fbbd08'
    },
    {
      title: '橄榄',
      name: 'olive',
      color: '#8dc63f'
    },
    {
      title: '森绿',
      name: 'green',
      color: '#39b54a'
    },
    {
      title: '天青',
      name: 'cyan',
      color: '#1cbbb4'
    },
    {
      title: '海蓝',
      name: 'blue',
      color: '#0081ff'
    },
    {
      title: '姹紫',
      name: 'purple',
      color: '#6739b6'
    },
    {
      title: '木槿',
      name: 'mauve',
      color: '#9c26b0'
    },
    {
      title: '桃粉',
      name: 'pink',
      color: '#e03997'
    },
    {
      title: '棕褐',
      name: 'brown',
      color: '#a5673f'
    },
    {
      title: '玄灰',
      name: 'grey',
      color: '#8799a3'
    },
    {
      title: '草灰',
      name: 'gray',
      color: '#aaaaaa'
    },
    {
      title: '墨黑',
      name: 'black',
      color: '#333333'
    },
    {
      title: '雅白',
      name: 'white',
      color: '#ffffff'
    },
    ]
  },

  onLoad: function (options) {
    this.getUserInfos();
  },
  onShow: function (options) {
    // console.log(222);
  },
  onReady: function (options) {
    // console.log(333);
  },
  // 页面滚动
  onPageScroll(event) {
    // this.setData({
    //   pageNumber: this.data.pageNumber + 1
    // })
    // this.queryList();
  },


    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
    },

  WodePage: function () {
    wx.navigateTo({
      url: '/pages/wode/wode'
    })
  },

  // 选择地区
  changeCountry(e) {
    this.setData({
      countryIndex: e.detail.value,
      diqu: this.data.countryList[e.detail.value],
      queryNullDetails: ' 暂无数据，可查询其他地区'
    });
    this.queryList();
  },
  // 搜索的输入
  inputTitleLike: function (e) {
    // console.log(e.detail.value)
    this.setData({
      titleLike: e.detail.value
    });
    console.log(this.data.titleLike)
  },


  //------获取微信code---------
  getOpenid() {
    var that = this;
    // console.log(app.globalData.wxOpenid)
    if (app.globalData.wxOpenid != '-') {
      return;
    }
    //------执行Login---------
    wx.login({
      success: res => {
        console.log('code：', res.code);
        wx.request({
          url: app.globalData.sysUrl + 'qrCode/weixin/getOpenid',
          data: {
            code: res.code
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          success: res => {
            console.log("请求应答: ", res.data);
            if (res.data.code == '0') {
              app.globalData.wxOpenid = res.data.msg;
            } else {
              wx.showToast({
                title: res.data.msg,
                icon: 'none',
              })
              that.getOpenid();
            }
          },
          fail: function (err) {
            console.log("获取失败: ", err);
            that.getOpenid();
          }
        })
      }
    });
  },


  getUserInfos(){
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
  
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({

      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              app.globalData.userInfo = res.userInfo
              this.setData({
                avatarUrl: res.userInfo.avatarUrl
              })
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
    // 获取系统状态栏信息
    wx.getSystemInfo({
      success: e => {
        app.globalData.StatusBar = e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          app.globalData.Custom = capsule;
          app.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          app.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    })
  },


})