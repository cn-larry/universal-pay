#! /bin/bash
# springboot的jar放同级目录下即可，只能有一个jar文件 (可能需要使用：chmod u+x run.sh)
# JAVA_HOME记得修改为自己服务器中的JAVA_HOME
JAVA_HOME=/usr/pay/java
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)
JAR=$(find . -maxdepth 1 -name "*.jar")
JAR="$CURRENT_PATH/${JAR:2}"
PID=$(ps -ef | grep $JAR | grep -v grep | awk '{ print $2 }')
PORT=80

case "$1" in
    -s)
        if [ ! -z "$PID" ]; then
            echo "$JAR 已经启动，进程号: $PID"
        else
            echo -n -e "启动 $JAR ... \n"
            nohup "$JAVA_HOME" -jar $JAR --server.port="$PORT" >/dev/null 2>&1 &
            if [ "$?"="0" ]; then
                echo "启动完成，请查看日志确保成功"
            else
                echo "启动失败"
            fi
        fi
        ;;
    -d)
        if [ -z "$PID" ]; then
            echo "$JAR 没有在运行，无需关闭"
        else
            echo "关闭 $JAR ..."
              kill -9 $PID
            if [ "$?"="0" ]; then
                echo "服务已关闭"
            else
                echo "服务关闭失败"
            fi
        fi
        ;;
    -r)
        ${0} -d
        ${0} -s
        ;;
    status)
        if [ ! -z "$PID" ]; then
            echo "$JAR 正在运行"
        else
            echo "$JAR 未在运行"
        fi
        ;;
    -l)
        tail -f logs/sys-info.log
        ;;
  *)
    echo "Usage: ./run.sh {-s|-d|-r|status|-l}" >&2
        exit 1
esac

