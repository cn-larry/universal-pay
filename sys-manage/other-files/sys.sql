-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`
(
    `table_id`          bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
    `table_name`        varchar(200)  DEFAULT '' COMMENT '表名称',
    `table_comment`     varchar(500)  DEFAULT '' COMMENT '表描述',
    `sub_table_name`    varchar(64)   DEFAULT NULL COMMENT '关联子表的表名',
    `sub_table_fk_name` varchar(64)   DEFAULT NULL COMMENT '子表关联的外键名',
    `class_name`        varchar(100)  DEFAULT '' COMMENT '实体类名称',
    `tpl_category`      varchar(200)  DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
    `package_name`      varchar(100)  DEFAULT NULL COMMENT '生成包路径',
    `module_name`       varchar(30)   DEFAULT NULL COMMENT '生成模块名',
    `business_name`     varchar(30)   DEFAULT NULL COMMENT '生成业务名',
    `function_name`     varchar(50)   DEFAULT NULL COMMENT '生成功能名',
    `function_author`   varchar(50)   DEFAULT NULL COMMENT '生成功能作者',
    `gen_type`          char(1)       DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
    `gen_path`          varchar(200)  DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
    `options`           varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
    `create_by`         varchar(64)   DEFAULT '' COMMENT '创建者',
    `create_time`       datetime      DEFAULT NULL COMMENT '创建时间',
    `update_by`         varchar(64)   DEFAULT '' COMMENT '更新者',
    `update_time`       datetime      DEFAULT NULL COMMENT '更新时间',
    `remark`            varchar(500)  DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表';


-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`
(
    `column_id`      bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
    `table_id`       varchar(64)  DEFAULT NULL COMMENT '归属表编号',
    `column_name`    varchar(200) DEFAULT NULL COMMENT '列名称',
    `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
    `column_type`    varchar(100) DEFAULT NULL COMMENT '列类型',
    `java_type`      varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
    `java_field`     varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
    `is_pk`          char(1)      DEFAULT NULL COMMENT '是否主键（1是）',
    `is_increment`   char(1)      DEFAULT NULL COMMENT '是否自增（1是）',
    `is_required`    char(1)      DEFAULT NULL COMMENT '是否必填（1是）',
    `is_insert`      char(1)      DEFAULT NULL COMMENT '是否为插入字段（1是）',
    `is_edit`        char(1)      DEFAULT NULL COMMENT '是否编辑字段（1是）',
    `is_list`        char(1)      DEFAULT NULL COMMENT '是否列表字段（1是）',
    `is_query`       char(1)      DEFAULT NULL COMMENT '是否查询字段（1是）',
    `query_type`     varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
    `html_type`      varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
    `dict_type`      varchar(200) DEFAULT '' COMMENT '字典类型',
    `sort`           int          DEFAULT NULL COMMENT '排序',
    `create_by`      varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time`    datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`      varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time`    datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表字段';

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`
(
    `config_id`    int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
    `config_name`  varchar(100) DEFAULT '' COMMENT '参数名称',
    `config_key`   varchar(100) DEFAULT '' COMMENT '参数键名',
    `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
    `config_type`  char(1)      DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
    `create_by`    varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time`  datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`    varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time`  datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`       varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='参数配置表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_config`
VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config`
VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '初始化密码 123456');
INSERT INTO `sys_config`
VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config`
VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config`
VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config`
VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config`
VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config`
VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config`
VALUES (9, '主框架页-是否开启页脚', 'sys.index.footer', 'true', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '是否开启底部页脚显示（true显示，false隐藏）');
INSERT INTO `sys_config`
VALUES (10, '主框架页-是否开启页签', 'sys.index.tagsView', 'true', 'Y', 'admin', '2021-11-10 14:03:51', '', NULL,
        '是否开启菜单多页签显示（true显示，false隐藏）');
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`
(
    `dept_id`     bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
    `parent_id`   bigint      DEFAULT '0' COMMENT '父部门id',
    `ancestors`   varchar(50) DEFAULT '' COMMENT '祖级列表',
    `dept_name`   varchar(30) DEFAULT '' COMMENT '部门名称',
    `order_num`   int         DEFAULT '0' COMMENT '显示顺序',
    `leader`      varchar(20) DEFAULT NULL COMMENT '负责人',
    `phone`       varchar(11) DEFAULT NULL COMMENT '联系电话',
    `email`       varchar(50) DEFAULT NULL COMMENT '邮箱',
    `status`      char(1)     DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
    `del_flag`    char(1)     DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_by`   varchar(64) DEFAULT '' COMMENT '创建者',
    `create_time` datetime    DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64) DEFAULT '' COMMENT '更新者',
    `update_time` datetime    DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept`
VALUES (100, 0, '0', '管理系统', 0, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin', '2021-11-10 14:03:51', '',
        NULL);
INSERT INTO `sys_dept`
VALUES (101, 100, '0,100', '北京总公司', 1, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin', '2021-11-10 14:03:51',
        '',
        NULL);
INSERT INTO `sys_dept`
VALUES (102, 100, '0,100', '长沙分公司', 2, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin', '2021-11-10 14:03:51',
        '',
        NULL);
INSERT INTO `sys_dept`
VALUES (103, 101, '0,100,101', '研发部门', 1, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
INSERT INTO `sys_dept`
VALUES (104, 101, '0,100,101', '市场部门', 2, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
INSERT INTO `sys_dept`
VALUES (105, 101, '0,100,101', '测试部门', 3, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
INSERT INTO `sys_dept`
VALUES (106, 101, '0,100,101', '财务部门', 4, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
INSERT INTO `sys_dept`
VALUES (107, 101, '0,100,101', '运维部门', 5, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
INSERT INTO `sys_dept`
VALUES (108, 102, '0,100,102', '市场部门', 1, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
INSERT INTO `sys_dept`
VALUES (109, 102, '0,100,102', '财务部门', 2, 'Larry', '15888888888', 'larry@qq.com', '0', '0', 'admin',
        '2021-11-10 14:03:51',
        '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`
(
    `dict_code`   bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
    `dict_sort`   int          DEFAULT '0' COMMENT '字典排序',
    `dict_label`  varchar(100) DEFAULT '' COMMENT '字典标签',
    `dict_value`  varchar(100) DEFAULT '' COMMENT '字典键值',
    `dict_type`   varchar(100) DEFAULT '' COMMENT '字典类型',
    `css_class`   varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
    `list_class`  varchar(100) DEFAULT NULL COMMENT '表格回显样式',
    `is_default`  char(1)      DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
    `status`      char(1)      DEFAULT '0' COMMENT '状态（0正常 1停用）',
    `create_by`   varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典数据表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_data`
VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '性别男');
INSERT INTO `sys_dict_data`
VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '性别女');
INSERT INTO `sys_dict_data`
VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '性别未知');
INSERT INTO `sys_dict_data`
VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data`
VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data`
VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL,
        '正常状态');
INSERT INTO `sys_dict_data`
VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL,
        '停用状态');
INSERT INTO `sys_dict_data`
VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '正常状态');
INSERT INTO `sys_dict_data`
VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '停用状态');
INSERT INTO `sys_dict_data`
VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '默认分组');
INSERT INTO `sys_dict_data`
VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '系统分组');
INSERT INTO `sys_dict_data`
VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data`
VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data`
VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '通知');
INSERT INTO `sys_dict_data`
VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '公告');
INSERT INTO `sys_dict_data`
VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-11-10 14:03:51', '', NULL,
        '正常状态');
INSERT INTO `sys_dict_data`
VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL,
        '关闭状态');
INSERT INTO `sys_dict_data`
VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '其他操作');
INSERT INTO `sys_dict_data`
VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '新增操作');
INSERT INTO `sys_dict_data`
VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '修改操作');
INSERT INTO `sys_dict_data`
VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '删除操作');
INSERT INTO `sys_dict_data`
VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '授权操作');
INSERT INTO `sys_dict_data`
VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '导出操作');
INSERT INTO `sys_dict_data`
VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '导入操作');
INSERT INTO `sys_dict_data`
VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '强退操作');
INSERT INTO `sys_dict_data`
VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '生成操作');
INSERT INTO `sys_dict_data`
VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '清空操作');
INSERT INTO `sys_dict_data`
VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL,
        '正常状态');
INSERT INTO `sys_dict_data`
VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-11-10 14:03:51', '', NULL,
        '停用状态');
INSERT INTO `sys_dict_data`
VALUES (100, 1, '商户资料审核中', 'UNDER_REVIEW', 'mer_state', '', 'default', 'Y', '0', 'admin', '2021-11-10 14:59:28',
        'admin', '2021-11-10 15:16:56', '商户资料审核中');
INSERT INTO `sys_dict_data`
VALUES (101, 2, '商户资料审核通过', 'APPROVED', 'mer_state', NULL, NULL, 'Y', '0', 'admin', '2021-11-10 14:59:49', '', NULL,
        '商户资料审核通过（可用的状态）');
INSERT INTO `sys_dict_data`
VALUES (102, 3, '商家营业中', 'NORMAL_BUSINESS', 'mer_state', '', 'primary', 'Y', '0', 'admin', '2021-11-10 15:00:04',
        'admin', '2021-11-10 15:17:15', '商家营业中');
INSERT INTO `sys_dict_data`
VALUES (103, 4, '商家休息', 'SHOP_REST', 'mer_state', '', 'warning', 'Y', '0', 'admin', '2021-11-10 15:00:17', 'admin',
        '2021-11-10 15:17:31', '商家休息');
INSERT INTO `sys_dict_data`
VALUES (104, 5, '商家已关闭', 'STORE_CLOSED', 'mer_state', '', 'danger', 'Y', '0', 'admin', '2021-11-10 15:00:36', 'admin',
        '2021-11-10 15:17:35', '商家已关闭（不营业）');
INSERT INTO `sys_dict_data`
VALUES (105, 1, '待支付', 'NOT_PAY', 'pay_state', '', 'default', 'Y', '0', 'admin', '2021-11-10 15:01:36', 'admin',
        '2021-11-10 15:17:53', '没有支付，待支付（一般交易刚创建成功，或者是创建成功了，消费者没有支付）');
INSERT INTO `sys_dict_data`
VALUES (106, 2, '交易失败', 'FAIL', 'pay_state', '', 'warning', 'Y', '0', 'admin', '2021-11-10 15:02:03', 'admin',
        '2021-11-10 15:17:59', '失败的，无效的订单（明确的支付失败，建议次日进行对账，防止上游交易渠道系统出错）');
INSERT INTO `sys_dict_data`
VALUES (107, 3, '交易已取消', 'CANCEL', 'pay_state', '', 'danger', 'Y', '0', 'admin', '2021-11-10 15:02:26', 'admin',
        '2021-11-10 15:19:44', '交易已取消（交易创建后未支付，取消了，可能是订单过了有效期，可能是主动取消的）');
INSERT INTO `sys_dict_data`
VALUES (108, 4, '交易已退款', 'REFUND', 'pay_state', '', 'danger', 'Y', '0', 'admin', '2021-11-10 15:02:55', 'admin',
        '2021-11-10 15:19:34', '交易已退款（全部退款）');
INSERT INTO `sys_dict_data`
VALUES (109, 5, '交易部分退款', 'PART_REFUND', 'pay_state', '', 'danger', 'Y', '0', 'admin', '2021-11-10 15:03:08', 'admin',
        '2021-11-10 15:19:39', '交易部分退款（不是全部退款）');
INSERT INTO `sys_dict_data`
VALUES (110, 6, '交易成功', 'SUCCESS', 'pay_state', NULL, 'success', 'Y', '0', 'admin', '2021-11-10 15:19:20', '', NULL,
        '交易成功');
INSERT INTO `sys_dict_data`
VALUES (111, 1, '正常', '1', 'pay_qrcode_status', NULL, 'success', 'Y', '0', 'admin', '2021-11-10 15:26:42', '', NULL,
        '正常可用');
INSERT INTO `sys_dict_data`
VALUES (112, 2, '停用', '0', 'pay_qrcode_status', NULL, 'danger', 'Y', '0', 'admin', '2021-11-10 15:27:01', '', NULL,
        '不可用');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`
(
    `dict_id`     bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
    `dict_name`   varchar(100) DEFAULT '' COMMENT '字典名称',
    `dict_type`   varchar(100) DEFAULT '' COMMENT '字典类型',
    `status`      char(1)      DEFAULT '0' COMMENT '状态（0正常 1停用）',
    `create_by`   varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`dict_id`),
    UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典类型表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_type`
VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type`
VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type`
VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type`
VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type`
VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type`
VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type`
VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type`
VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type`
VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type`
VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type`
VALUES (100, '商户状态', 'mer_state', '0', 'admin', '2021-11-10 14:58:47', 'admin', '2021-11-10 14:58:59', '商户状态');
INSERT INTO `sys_dict_type`
VALUES (101, '交易状态', 'pay_state', '0', 'admin', '2021-11-10 15:01:04', '', NULL, '交易状态');
INSERT INTO `sys_dict_type`
VALUES (102, '收款码状态', 'pay_qrcode_status', '0', 'admin', '2021-11-10 15:26:07', '', NULL, '收款码状态');
COMMIT;

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`
(
    `job_id`          bigint       NOT NULL AUTO_INCREMENT COMMENT '任务ID',
    `job_name`        varchar(64)  NOT NULL DEFAULT '' COMMENT '任务名称',
    `job_group`       varchar(64)  NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
    `invoke_target`   varchar(500) NOT NULL COMMENT '调用目标字符串',
    `cron_expression` varchar(255)          DEFAULT '' COMMENT 'cron执行表达式',
    `misfire_policy`  varchar(20)           DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
    `concurrent`      char(1)               DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
    `status`          char(1)               DEFAULT '0' COMMENT '状态（0正常 1暂停）',
    `create_by`       varchar(64)           DEFAULT '' COMMENT '创建者',
    `create_time`     datetime              DEFAULT NULL COMMENT '创建时间',
    `update_by`       varchar(64)           DEFAULT '' COMMENT '更新者',
    `update_time`     datetime              DEFAULT NULL COMMENT '更新时间',
    `remark`          varchar(500)          DEFAULT '' COMMENT '备注信息',
    PRIMARY KEY (`job_id`, `job_name`, `job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务调度表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_job`
VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_job`
VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'larry\')', '0/15 * * * * ?', '3', '1', '1', 'admin',
        '2021-11-10 14:03:51', '', NULL, '');
INSERT INTO `sys_job`
VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'larry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?',
        '3',
        '1', '1', 'admin', '2021-11-10 14:03:51', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`
(
    `job_log_id`     bigint       NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
    `job_name`       varchar(64)  NOT NULL COMMENT '任务名称',
    `job_group`      varchar(64)  NOT NULL COMMENT '任务组名',
    `invoke_target`  varchar(500) NOT NULL COMMENT '调用目标字符串',
    `job_message`    varchar(500)  DEFAULT NULL COMMENT '日志信息',
    `status`         char(1)       DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
    `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
    `create_time`    datetime      DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务调度日志表';


-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`
(
    `info_id`        bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
    `login_name`     varchar(50)  DEFAULT '' COMMENT '登录账号',
    `ipaddr`         varchar(128) DEFAULT '' COMMENT '登录IP地址',
    `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
    `browser`        varchar(50)  DEFAULT '' COMMENT '浏览器类型',
    `os`             varchar(50)  DEFAULT '' COMMENT '操作系统',
    `status`         char(1)      DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
    `msg`            varchar(255) DEFAULT '' COMMENT '提示消息',
    `login_time`     datetime     DEFAULT NULL COMMENT '访问时间',
    PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统访问记录';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `menu_id`     bigint      NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
    `menu_name`   varchar(50) NOT NULL COMMENT '菜单名称',
    `parent_id`   bigint       DEFAULT '0' COMMENT '父菜单ID',
    `order_num`   int          DEFAULT '0' COMMENT '显示顺序',
    `url`         varchar(200) DEFAULT '#' COMMENT '请求地址',
    `target`      varchar(20)  DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
    `menu_type`   char(1)      DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
    `visible`     char(1)      DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
    `is_refresh`  char(1)      DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
    `perms`       varchar(100) DEFAULT NULL COMMENT '权限标识',
    `icon`        varchar(100) DEFAULT '#' COMMENT '菜单图标',
    `create_by`   varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20031 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单权限表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu`
VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-11-10 14:03:51', '', NULL, '系统管理目录');
INSERT INTO `sys_menu`
VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-11-10 14:03:51', '', NULL,
        '系统监控目录');
INSERT INTO `sys_menu`
VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2021-11-10 14:03:51', '', NULL, '系统工具目录');
INSERT INTO `sys_menu`
VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', 'menuBlank', 'C', '0', '1', '', 'fa fa-location-arrow', 'admin',
        '2021-11-10 14:03:51', '', NULL, '若依官网地址');
INSERT INTO `sys_menu`
VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin',
        '2021-11-10 14:03:51', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu`
VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin',
        '2021-11-10 14:03:51', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu`
VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin',
        '2021-11-10 14:03:51', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu`
VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin',
        '2021-11-10 14:03:51', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu`
VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin',
        '2021-11-10 14:03:51', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu`
VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin',
        '2021-11-10 14:03:51', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu`
VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin',
        '2021-11-10 14:03:51', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu`
VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin',
        '2021-11-10 14:03:51', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu`
VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-11-10 14:03:51', '',
        NULL, '日志管理菜单');
INSERT INTO `sys_menu`
VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin',
        '2021-11-10 14:03:51', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu`
VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin',
        '2021-11-10 14:03:51', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu`
VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin',
        '2021-11-10 14:03:51', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu`
VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin',
        '2021-11-10 14:03:51', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu`
VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin',
        '2021-11-10 14:03:51', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu`
VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin',
        '2021-11-10 14:03:51', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu`
VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin',
        '2021-11-10 14:03:51', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu`
VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin',
        '2021-11-10 14:03:51', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu`
VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book',
        'admin', '2021-11-10 14:03:51', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu`
VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o',
        'admin', '2021-11-10 14:03:51', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu`
VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin',
        '2021-11-10 14:03:51', '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2021-11-10 14:03:51',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2021-11-10 14:03:51', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2021-11-10 14:03:51', '', NULL,
        '');
INSERT INTO `sys_menu`
VALUES (2000, '交易管理', 0, 5, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-hand-stop-o', 'admin', '2021-11-10 14:33:59',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2001, '商户管理', 0, 6, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-users', 'admin', '2021-11-10 14:34:26', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (2002, '商户审核', 2001, 1, '/pay/merInfo', 'menuItem', 'C', '0', '0', 'pay:merInfo:view', '#', 'admin',
        '2021-11-10 14:48:11', '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2003, '商户信息', 2001, 2, '/pay/merInfo', 'menuItem', 'C', '0', '0', 'pay:merInfo:view', '#', 'admin',
        '2021-11-10 14:49:07', '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2004, '收款码', 2001, 3, '/pay/qrcode', 'menuItem', 'C', '0', '0', 'pay:qrcode:view', '#', 'admin',
        '2021-11-10 14:49:42', 'admin', '2021-11-10 15:40:37', '');
INSERT INTO `sys_menu`
VALUES (2005, '查询', 2002, 1, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:list', '#', 'admin', '2021-11-10 14:50:39',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2006, '导出', 2002, 2, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:export', '#', 'admin', '2021-11-10 14:51:06',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2007, '新增', 2002, 3, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:add', '#', 'admin', '2021-11-10 14:53:47', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (2008, '修改', 2002, 4, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:edit', '#', 'admin', '2021-11-10 14:54:06',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2009, '删除', 2002, 5, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:remove', '#', 'admin', '2021-11-10 14:54:21',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2010, '查询', 2003, 1, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:list', '#', 'admin', '2021-11-10 14:50:39',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2011, '导出', 2003, 2, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:export', '#', 'admin', '2021-11-10 14:51:06',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2012, '新增', 2003, 3, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:add', '#', 'admin', '2021-11-10 14:53:47', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (2013, '修改', 2003, 4, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:edit', '#', 'admin', '2021-11-10 14:54:06',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (2014, '删除', 2003, 5, '#', 'menuItem', 'F', '0', '1', 'pay:merInfo:remove', '#', 'admin', '2021-11-10 14:54:21',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20014, '查询', 2004, 1, '#', 'menuItem', 'F', '0', '1', 'pay:qrcode:list', '#', 'admin', '2021-11-10 15:40:55',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20015, '导出', 2004, 2, '#', 'menuItem', 'F', '0', '1', 'pay:qrcode:export', '#', 'admin', '2021-11-10 15:41:10',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20016, '新增', 2004, 3, '#', 'menuItem', 'F', '0', '1', 'pay:qrcode:add', '#', 'admin', '2021-11-10 15:41:28', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (20017, '修改', 2004, 4, '#', 'menuItem', 'F', '0', '1', 'pay:qrcode:edit', '#', 'admin', '2021-11-10 15:41:44',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20018, '删除', 2004, 5, '#', 'menuItem', 'F', '0', '1', 'pay:qrcode:remove', '#', 'admin', '2021-11-10 15:41:58',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20019, '交易记录', 2000, 1, '/pay/order', 'menuItem', 'C', '0', '0', 'pay:order:view', '#', 'admin',
        '2021-11-10 15:43:37', 'admin', '2021-11-10 15:57:08', '');
INSERT INTO `sys_menu`
VALUES (20020, '退款记录', 2000, 2, '/pay/refund', 'menuItem', 'C', '0', '0', 'pay:refund:view', '#', 'admin',
        '2021-11-10 15:43:49', 'admin', '2021-11-10 18:27:09', '');
INSERT INTO `sys_menu`
VALUES (20021, '查询', 20019, 1, '#', 'menuItem', 'F', '0', '1', 'pay:order:list', '#', 'admin', '2021-11-10 15:57:25',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20022, '导出', 20019, 2, '#', 'menuItem', 'F', '0', '1', 'pay:order:export', '#', 'admin', '2021-11-10 15:57:40',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20023, '新增', 20019, 3, '#', 'menuItem', 'F', '0', '1', 'pay:order:add', '#', 'admin', '2021-11-10 15:58:08', '',
        NULL, '');
INSERT INTO `sys_menu`
VALUES (20024, '修改', 20019, 4, '#', 'menuItem', 'F', '0', '1', 'pay:order:edit', '#', 'admin', '2021-11-10 15:58:27',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20025, '删除', 20019, 5, '#', 'menuItem', 'F', '0', '1', 'pay:order:remove', '#', 'admin', '2021-11-10 15:58:45',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20026, '查询', 20020, 1, '#', 'menuItem', 'F', '0', '1', 'pay:refund:list', '#', 'admin', '2021-11-10 18:27:30',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20027, '导出', 20020, 2, '#', 'menuItem', 'F', '0', '1', 'pay:refund:export', '#', 'admin', '2021-11-10 18:27:43',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20028, '新增', 20020, 3, '#', 'menuItem', 'F', '0', '1', 'pay:refund:add', '#', 'admin', '2021-11-10 18:28:06',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20029, '修改', 20020, 4, '#', 'menuItem', 'F', '0', '1', 'pay:refund:edit', '#', 'admin', '2021-11-10 18:28:29',
        '', NULL, '');
INSERT INTO `sys_menu`
VALUES (20030, '删除', 20020, 5, '#', 'menuItem', 'F', '0', '1', 'pay:refund:remove', '#', 'admin', '2021-11-10 18:28:53',
        'admin', '2021-11-10 18:45:34', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`
(
    `notice_id`      int         NOT NULL AUTO_INCREMENT COMMENT '公告ID',
    `notice_title`   varchar(50) NOT NULL COMMENT '公告标题',
    `notice_type`    char(1)     NOT NULL COMMENT '公告类型（1通知 2公告）',
    `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
    `status`         char(1)       DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
    `create_by`      varchar(64)   DEFAULT '' COMMENT '创建者',
    `create_time`    datetime      DEFAULT NULL COMMENT '创建时间',
    `update_by`      varchar(64)   DEFAULT '' COMMENT '更新者',
    `update_time`    datetime      DEFAULT NULL COMMENT '更新时间',
    `remark`         varchar(255)  DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='通知公告表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_notice`
VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '管理员');
INSERT INTO `sys_notice`
VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '管理员');
COMMIT;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`
(
    `oper_id`        bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
    `title`          varchar(50)   DEFAULT '' COMMENT '模块标题',
    `business_type`  int           DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
    `method`         varchar(100)  DEFAULT '' COMMENT '方法名称',
    `request_method` varchar(10)   DEFAULT '' COMMENT '请求方式',
    `operator_type`  int           DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
    `oper_name`      varchar(50)   DEFAULT '' COMMENT '操作人员',
    `dept_name`      varchar(50)   DEFAULT '' COMMENT '部门名称',
    `oper_url`       varchar(255)  DEFAULT '' COMMENT '请求URL',
    `oper_ip`        varchar(128)  DEFAULT '' COMMENT '主机地址',
    `oper_location`  varchar(255)  DEFAULT '' COMMENT '操作地点',
    `oper_param`     varchar(2000) DEFAULT '' COMMENT '请求参数',
    `json_result`    varchar(2000) DEFAULT '' COMMENT '返回参数',
    `status`         int           DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
    `error_msg`      varchar(2000) DEFAULT '' COMMENT '错误消息',
    `oper_time`      datetime      DEFAULT NULL COMMENT '操作时间',
    PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='操作日志记录';

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`
(
    `post_id`     bigint      NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
    `post_code`   varchar(64) NOT NULL COMMENT '岗位编码',
    `post_name`   varchar(50) NOT NULL COMMENT '岗位名称',
    `post_sort`   int         NOT NULL COMMENT '显示顺序',
    `status`      char(1)     NOT NULL COMMENT '状态（0正常 1停用）',
    `create_by`   varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='岗位信息表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_post`
VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-11-10 14:03:51', '', NULL, '');
INSERT INTO `sys_post`
VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2021-11-10 14:03:51', '', NULL, '');
INSERT INTO `sys_post`
VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-11-10 14:03:51', '', NULL, '');
INSERT INTO `sys_post`
VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-11-10 14:03:51', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `role_id`     bigint       NOT NULL AUTO_INCREMENT COMMENT '角色ID',
    `role_name`   varchar(30)  NOT NULL COMMENT '角色名称',
    `role_key`    varchar(100) NOT NULL COMMENT '角色权限字符串',
    `role_sort`   int          NOT NULL COMMENT '显示顺序',
    `data_scope`  char(1)      DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
    `status`      char(1)      NOT NULL COMMENT '角色状态（0正常 1停用）',
    `del_flag`    char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_by`   varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色信息表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_role`
VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '超级管理员');
INSERT INTO `sys_role`
VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2021-11-10 14:03:51', '', NULL, '普通角色');
INSERT INTO `sys_role`
VALUES (100, '交易系统功能', 'pay', 3, '1', '0', '0', 'admin', '2021-11-10 14:46:30', 'admin', '2021-11-10 19:04:15',
        '仅有交易相关功能权限');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`
(
    `role_id` bigint NOT NULL COMMENT '角色ID',
    `dept_id` bigint NOT NULL COMMENT '部门ID',
    PRIMARY KEY (`role_id`, `dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色和部门关联表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_dept`
VALUES (2, 100);
INSERT INTO `sys_role_dept`
VALUES (2, 101);
INSERT INTO `sys_role_dept`
VALUES (2, 105);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `role_id` bigint NOT NULL COMMENT '角色ID',
    `menu_id` bigint NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (`role_id`, `menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色和菜单关联表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu`
VALUES (2, 1);
INSERT INTO `sys_role_menu`
VALUES (2, 2);
INSERT INTO `sys_role_menu`
VALUES (2, 3);
INSERT INTO `sys_role_menu`
VALUES (2, 4);
INSERT INTO `sys_role_menu`
VALUES (2, 100);
INSERT INTO `sys_role_menu`
VALUES (2, 101);
INSERT INTO `sys_role_menu`
VALUES (2, 102);
INSERT INTO `sys_role_menu`
VALUES (2, 103);
INSERT INTO `sys_role_menu`
VALUES (2, 104);
INSERT INTO `sys_role_menu`
VALUES (2, 105);
INSERT INTO `sys_role_menu`
VALUES (2, 106);
INSERT INTO `sys_role_menu`
VALUES (2, 107);
INSERT INTO `sys_role_menu`
VALUES (2, 108);
INSERT INTO `sys_role_menu`
VALUES (2, 109);
INSERT INTO `sys_role_menu`
VALUES (2, 110);
INSERT INTO `sys_role_menu`
VALUES (2, 111);
INSERT INTO `sys_role_menu`
VALUES (2, 112);
INSERT INTO `sys_role_menu`
VALUES (2, 113);
INSERT INTO `sys_role_menu`
VALUES (2, 114);
INSERT INTO `sys_role_menu`
VALUES (2, 115);
INSERT INTO `sys_role_menu`
VALUES (2, 116);
INSERT INTO `sys_role_menu`
VALUES (2, 500);
INSERT INTO `sys_role_menu`
VALUES (2, 501);
INSERT INTO `sys_role_menu`
VALUES (2, 1000);
INSERT INTO `sys_role_menu`
VALUES (2, 1001);
INSERT INTO `sys_role_menu`
VALUES (2, 1002);
INSERT INTO `sys_role_menu`
VALUES (2, 1003);
INSERT INTO `sys_role_menu`
VALUES (2, 1004);
INSERT INTO `sys_role_menu`
VALUES (2, 1005);
INSERT INTO `sys_role_menu`
VALUES (2, 1006);
INSERT INTO `sys_role_menu`
VALUES (2, 1007);
INSERT INTO `sys_role_menu`
VALUES (2, 1008);
INSERT INTO `sys_role_menu`
VALUES (2, 1009);
INSERT INTO `sys_role_menu`
VALUES (2, 1010);
INSERT INTO `sys_role_menu`
VALUES (2, 1011);
INSERT INTO `sys_role_menu`
VALUES (2, 1012);
INSERT INTO `sys_role_menu`
VALUES (2, 1013);
INSERT INTO `sys_role_menu`
VALUES (2, 1014);
INSERT INTO `sys_role_menu`
VALUES (2, 1015);
INSERT INTO `sys_role_menu`
VALUES (2, 1016);
INSERT INTO `sys_role_menu`
VALUES (2, 1017);
INSERT INTO `sys_role_menu`
VALUES (2, 1018);
INSERT INTO `sys_role_menu`
VALUES (2, 1019);
INSERT INTO `sys_role_menu`
VALUES (2, 1020);
INSERT INTO `sys_role_menu`
VALUES (2, 1021);
INSERT INTO `sys_role_menu`
VALUES (2, 1022);
INSERT INTO `sys_role_menu`
VALUES (2, 1023);
INSERT INTO `sys_role_menu`
VALUES (2, 1024);
INSERT INTO `sys_role_menu`
VALUES (2, 1025);
INSERT INTO `sys_role_menu`
VALUES (2, 1026);
INSERT INTO `sys_role_menu`
VALUES (2, 1027);
INSERT INTO `sys_role_menu`
VALUES (2, 1028);
INSERT INTO `sys_role_menu`
VALUES (2, 1029);
INSERT INTO `sys_role_menu`
VALUES (2, 1030);
INSERT INTO `sys_role_menu`
VALUES (2, 1031);
INSERT INTO `sys_role_menu`
VALUES (2, 1032);
INSERT INTO `sys_role_menu`
VALUES (2, 1033);
INSERT INTO `sys_role_menu`
VALUES (2, 1034);
INSERT INTO `sys_role_menu`
VALUES (2, 1035);
INSERT INTO `sys_role_menu`
VALUES (2, 1036);
INSERT INTO `sys_role_menu`
VALUES (2, 1037);
INSERT INTO `sys_role_menu`
VALUES (2, 1038);
INSERT INTO `sys_role_menu`
VALUES (2, 1039);
INSERT INTO `sys_role_menu`
VALUES (2, 1040);
INSERT INTO `sys_role_menu`
VALUES (2, 1041);
INSERT INTO `sys_role_menu`
VALUES (2, 1042);
INSERT INTO `sys_role_menu`
VALUES (2, 1043);
INSERT INTO `sys_role_menu`
VALUES (2, 1044);
INSERT INTO `sys_role_menu`
VALUES (2, 1045);
INSERT INTO `sys_role_menu`
VALUES (2, 1046);
INSERT INTO `sys_role_menu`
VALUES (2, 1047);
INSERT INTO `sys_role_menu`
VALUES (2, 1048);
INSERT INTO `sys_role_menu`
VALUES (2, 1049);
INSERT INTO `sys_role_menu`
VALUES (2, 1050);
INSERT INTO `sys_role_menu`
VALUES (2, 1051);
INSERT INTO `sys_role_menu`
VALUES (2, 1052);
INSERT INTO `sys_role_menu`
VALUES (2, 1053);
INSERT INTO `sys_role_menu`
VALUES (2, 1054);
INSERT INTO `sys_role_menu`
VALUES (2, 1055);
INSERT INTO `sys_role_menu`
VALUES (2, 1056);
INSERT INTO `sys_role_menu`
VALUES (2, 1057);
INSERT INTO `sys_role_menu`
VALUES (2, 1058);
INSERT INTO `sys_role_menu`
VALUES (2, 1059);
INSERT INTO `sys_role_menu`
VALUES (2, 1060);
INSERT INTO `sys_role_menu`
VALUES (2, 1061);
INSERT INTO `sys_role_menu`
VALUES (100, 2000);
INSERT INTO `sys_role_menu`
VALUES (100, 2001);
INSERT INTO `sys_role_menu`
VALUES (100, 2003);
INSERT INTO `sys_role_menu`
VALUES (100, 2004);
INSERT INTO `sys_role_menu`
VALUES (100, 2010);
INSERT INTO `sys_role_menu`
VALUES (100, 2011);
INSERT INTO `sys_role_menu`
VALUES (100, 20014);
INSERT INTO `sys_role_menu`
VALUES (100, 20015);
INSERT INTO `sys_role_menu`
VALUES (100, 20019);
INSERT INTO `sys_role_menu`
VALUES (100, 20020);
INSERT INTO `sys_role_menu`
VALUES (100, 20021);
INSERT INTO `sys_role_menu`
VALUES (100, 20022);
INSERT INTO `sys_role_menu`
VALUES (100, 20026);
INSERT INTO `sys_role_menu`
VALUES (100, 20027);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `user_id`         bigint      NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `dept_id`         bigint       DEFAULT NULL COMMENT '部门ID',
    `login_name`      varchar(30) NOT NULL COMMENT '登录账号',
    `user_name`       varchar(30)  DEFAULT '' COMMENT '用户昵称',
    `user_type`       varchar(2)   DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
    `email`           varchar(50)  DEFAULT '' COMMENT '用户邮箱',
    `phonenumber`     varchar(11)  DEFAULT '' COMMENT '手机号码',
    `sex`             char(1)      DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
    `avatar`          varchar(100) DEFAULT '' COMMENT '头像路径',
    `password`        varchar(50)  DEFAULT '' COMMENT '密码',
    `salt`            varchar(20)  DEFAULT '' COMMENT '盐加密',
    `status`          char(1)      DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
    `del_flag`        char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `login_ip`        varchar(128) DEFAULT '' COMMENT '最后登录IP',
    `login_date`      datetime     DEFAULT NULL COMMENT '最后登录时间',
    `pwd_update_date` datetime     DEFAULT NULL COMMENT '密码最后更新时间',
    `create_by`       varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time`     datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`       varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time`     datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`          varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_user`
VALUES (1, 103, 'admin', '管理员', '00', 'larry@qq.com', '15888888888', '1', '', 'cNeYQsQE5n5s8MGo6tw59A==', '111111', '0',
        '0', '127.0.0.1', '2021-11-11 10:22:10', '2021-11-10 14:03:51', 'admin', '2021-11-10 14:03:51', '',
        '2021-11-11 10:22:10', '管理员');
INSERT INTO `sys_user`
VALUES (100, 102, 'llin', 'llin', '00', '', '', '0', '', 'Yn+4rgbmkxBLCvNZWIQHHQ==', 'b2c76d', '0', '0', '127.0.0.1',
        '2021-11-11 10:13:57', NULL, 'admin', '2021-11-10 14:47:11', '', '2021-11-11 10:13:57', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`
(
    `sessionId`        varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
    `login_name`       varchar(50)          DEFAULT '' COMMENT '登录账号',
    `dept_name`        varchar(50)          DEFAULT '' COMMENT '部门名称',
    `ipaddr`           varchar(128)         DEFAULT '' COMMENT '登录IP地址',
    `login_location`   varchar(255)         DEFAULT '' COMMENT '登录地点',
    `browser`          varchar(50)          DEFAULT '' COMMENT '浏览器类型',
    `os`               varchar(50)          DEFAULT '' COMMENT '操作系统',
    `status`           varchar(10)          DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
    `start_timestamp`  datetime             DEFAULT NULL COMMENT 'session创建时间',
    `last_access_time` datetime             DEFAULT NULL COMMENT 'session最后访问时间',
    `expire_time`      int                  DEFAULT '0' COMMENT '超时时间，单位为分钟',
    PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='在线用户记录';

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`
(
    `user_id` bigint NOT NULL COMMENT '用户ID',
    `post_id` bigint NOT NULL COMMENT '岗位ID',
    PRIMARY KEY (`user_id`, `post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户与岗位关联表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_post`
VALUES (1, 1);
INSERT INTO `sys_user_post`
VALUES (2, 2);
INSERT INTO `sys_user_post`
VALUES (100, 2);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `user_id` bigint NOT NULL COMMENT '用户ID',
    `role_id` bigint NOT NULL COMMENT '角色ID',
    PRIMARY KEY (`user_id`, `role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户和角色关联表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role`
VALUES (1, 1);
INSERT INTO `sys_user_role`
VALUES (2, 2);
INSERT INTO `sys_user_role`
VALUES (100, 100);
COMMIT;

