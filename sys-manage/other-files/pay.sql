-- ----------------------------
-- 1、商户表
-- ----------------------------
DROP TABLE IF EXISTS `pay_mer`;
CREATE TABLE `pay_mer`
(
    `mer_id`       bigint       NOT NULL AUTO_INCREMENT COMMENT '商户ID',
    `mer_name`     varchar(100) NOT NULL COMMENT '商户名称',
    `mer_sub_name` varchar(50)  NOT NULL COMMENT '商户简称',
    `mer_status`   varchar(20)  DEFAULT 'UNDER_REVIEW' COMMENT '商户状态（0正常 1停用）',
    `del_flag`     char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_by`    varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time`  datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`    varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time`  datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`       varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`mer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商户表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `pay_mer`
VALUES (100, '天津狗不理包子铺', '狗不理包子', 'UNDER_REVIEW', '', '', '2021-11-10 15:14:54', '', '2021-11-10 15:16:18', '');
INSERT INTO `pay_mer`
VALUES (101, '西安肉夹馍', '西安肉夹馍', 'UNDER_REVIEW', '', '', '2021-11-10 15:15:29', '', NULL, '');
INSERT INTO `pay_mer`
VALUES (102, '杭州叫花鸡路边摊', '杭州叫花鸡', 'NORMAL_BUSINESS', '', '', '2021-11-10 15:16:03', '', '2021-11-10 15:20:06', '');
COMMIT;

-- ----------------------------
-- 2、商户交易参数表
-- ----------------------------
DROP TABLE IF EXISTS `pay_mer_payparameter`;
CREATE TABLE `pay_mer_payparameter`
(
    `mer_id`        bigint       NOT NULL AUTO_INCREMENT COMMENT '商户ID',
    `pay_code`      varchar(10)  NOT NULL COMMENT '支付code',
    `pay_parameter` varchar(100) NOT NULL COMMENT '支付参数',
    `del_flag`      char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_by`     varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time`   datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`     varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time`   datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`        varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`mer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商户交易参数表';

-- ----------------------------
-- 初始化数据
-- ----------------------------
BEGIN;
INSERT INTO `pay_mer_payparameter`
VALUES (100, '11', '33', '', '', NULL, '', NULL, '');
INSERT INTO `pay_mer_payparameter`
VALUES (101, '11', '22', '', '', NULL, '', NULL, '');
INSERT INTO `pay_mer_payparameter`
VALUES (102, '11', '89', '', '', NULL, '', NULL, '');
COMMIT;

-- ----------------------------
-- 3、商户静态收款码表
-- ----------------------------
DROP TABLE IF EXISTS `pay_mer_static_qrcode`;
CREATE TABLE `pay_mer_static_qrcode`
(
    `qrcode_id`     varchar(15) NOT NULL COMMENT '收款码ID',
    `qrcode_status` char(1)      DEFAULT '1' COMMENT '收款码状态（1正常 0停用）',
    `mer_id`        bigint      NOT NULL COMMENT '商户ID',
    `del_flag`      char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_by`     varchar(64)  DEFAULT '' COMMENT '创建者',
    `create_time`   datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`     varchar(64)  DEFAULT '' COMMENT '更新者',
    `update_time`   datetime     DEFAULT NULL COMMENT '更新时间',
    `remark`        varchar(500) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`qrcode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商户静态收款码表';


-- ----------------------------
-- 4、交易表
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order`
(
    `id`             bigint      NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `order_time`     datetime     DEFAULT NULL COMMENT '订单时间',
    `pay_time`       datetime     DEFAULT NULL COMMENT '订单支付时间',
    `pay_type`       varchar(10) NOT NULL COMMENT '交易类型',
    `pay_no`         varchar(50) NOT NULL COMMENT '系统交易订单号',
    `channel_mer_no` varchar(50)  DEFAULT NULL COMMENT '渠道商户交易单号',
    `channel_pay_no` varchar(50)  DEFAULT NULL COMMENT '渠道支付单号',
    `pay_user_id`    varchar(50)  DEFAULT NULL COMMENT '消费者用户ID',
    `mer_id`         bigint      NOT NULL COMMENT '商户ID',
    `qrcode_id`      varchar(15)  DEFAULT NULL COMMENT '收款码ID',
    `pay_status`     varchar(20)  DEFAULT 'NOT_PAY' COMMENT '交易状态',
    `pay_amt`        bigint      NOT NULL COMMENT '订单金额（单位：分）',
    `remark`         varchar(500) DEFAULT NULL COMMENT '交易备注',
    `del_flag`       char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_time`    datetime     DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `id_01` (`pay_no`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='交易表';


-- ----------------------------
-- 5、退款表
-- ----------------------------
DROP TABLE IF EXISTS `pay_order_refund`;
CREATE TABLE `pay_order_refund`
(
    `id`            bigint      NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `refund_no`     varchar(50) NOT NULL COMMENT '退款订单号',
    `pay_no`        varchar(50) NOT NULL COMMENT '原交易系统订单号',
    `refund_status` varchar(20)  DEFAULT NULL COMMENT '退款状态',
    `refund_amt`    bigint      NOT NULL COMMENT '退款金额',
    `remark`        varchar(500) DEFAULT NULL COMMENT '退款备注',
    `del_flag`      char(1)      DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_time`   datetime     DEFAULT NULL COMMENT '创建时间',
    `update_time`   datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='退款表';

