package cn.llin.common.enums;

/**
 * 操作人类别
 * 
 * @author sys-manage
 */
public enum OperatorType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
