package cn.llin.common.enums;

/**
 * 数据源
 * 
 * @author sys-manage
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
