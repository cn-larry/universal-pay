package cn.llin.weixin.enums;

/**
 * Larry
 * 2021/11/2 13:58
 *
 * @Version 1.0
 */
public enum WeixinLoginImageStateEnum {


    /**
     * （有效）未登录的二维码
     */
    NOT_LOGGED,

    /**
     * 失效的二维码
     */
    INVALID,

    /**
     * （不能二次扫码）已经扫码过了
     */
    CODE_SCANNED,

    /**
     * 登录成功
     */
    LOGIN_SUCCEEDED

}
