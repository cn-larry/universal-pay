package cn.llin.weixin.utils;

import cn.llin.common.utils.http.HttpUtils;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.net.URLEncoder;

/**
 * Larry
 * 2021/11/7 14:30
 *
 * @Version 1.0
 */
@Slf4j
public class WeixinUtil {

    private static final String WEIXIN_AUTHURL = "https://open.weixin.qq.com/connect/oauth2/authorize";
    private static final String WEIXIN_AUTHPARAM = "response_type=code&scope=snsapi_base&state=STATE&connect_redirect=1#wechat_redirect";

    private static final String WEIXIN_TOKENURL = "https://api.weixin.qq.com/sns/oauth2/access_token";

    /**
     * 根据用户授权code获取用户openid
     *
     * @param appid
     * @param code
     * @param secret
     * @return
     */
    public static String getWeinxinOpenid(String appid, String code, String secret) {
        String param = "appid=" + appid + "&secret=" + secret + "&code=" + code
                + "&grant_type=authorization_code";
        String body = HttpUtils.sendGet(WEIXIN_TOKENURL, param);

        JSONObject jsonObject = JSONObject.parseObject(body);
        log.info("微信应答：{}", jsonObject.toString());
        return jsonObject.getString("openid");
    }

    /**
     * 组微信静默授权地址
     *
     * @param redirectUri
     * @param appid
     * @return
     * @throws Exception
     */
    public static String getRedirectUrl(String redirectUri, String appid){
        try {
            String url = WEIXIN_AUTHURL + "?appid=" + appid;
            redirectUri = URLEncoder.encode(redirectUri, "UTF-8");
            return url + "&redirect_uri=" + redirectUri + "&" + WEIXIN_AUTHPARAM;
        } catch (Exception e) {
        }
        return null;
    }


}
