package cn.llin.weixin.service;

import cn.llin.common.core.domain.entity.SysUser;
import cn.llin.weixin.enums.WeixinLoginImageStateEnum;

/**
 * Larry
 * 2021/11/2 13:42
 * 微信登录
 *
 * @Version 1.0
 */
public interface WeixinLoginService {

    /**
     * 获取登录的URL
     *
     * @param loginImageId
     * @return
     */
    public String getLoginImageUrl(String loginImageId);

    /**
     * 查询微信登录二维码状态
     *
     * @param loginImageId
     * @return
     */
    public WeixinLoginImageStateEnum selectLoginImageUrlState(String loginImageId);

    /**
     * 根据微信用户临时授权code获取微信用户的openid
     *
     * @param code
     * @param loginImageId
     * @return
     */
    public String getUserOpenid(String code, String loginImageId);

    /**
     * 根据微信用户的openid获取用户信息
     *
     * @param openid
     * @param loginImageId
     * @return
     */
    public SysUser getSysUserByOpenid(String openid, String loginImageId);

    /**
     * 根据微信用户临时授权code获取微信用户信息
     *
     * @param code
     * @param loginImageId
     * @return
     */
    public SysUser getSysUserBycode(String code, String loginImageId);

    public void setRedis(String loginImageId, SysUser sysUser);

    public SysUser getRedis(String loginImageId);

}
