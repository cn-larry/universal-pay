package cn.llin.weixin.service.impl;

import cn.llin.common.core.domain.entity.SysUser;
import cn.llin.framework.shiro.service.SysPasswordService;
import cn.llin.system.service.ISysUserService;
import cn.llin.weixin.config.WeixinConfig;
import cn.llin.weixin.enums.WeixinLoginImageStateEnum;
import cn.llin.weixin.service.WeixinLoginService;
import cn.llin.weixin.utils.WeixinUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * Larry
 * 2021/11/2 14:12
 *
 * @Version 1.0
 */
@Slf4j
@Service
public class WeixinLoginServiceImpl implements WeixinLoginService {

    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    ISysUserService sysUserService;
    @Autowired
    SysPasswordService sysPasswordService;
    @Autowired
    WeixinConfig weixinConfig;

    /**
     * 获取登录的URL
     *
     * @param loginImageId
     * @return
     */
    @Override
    public String getLoginImageUrl(String loginImageId) {
        if (StringUtils.isBlank(loginImageId))
            return null;
        if (redisTemplate.hasKey(loginImageId))
            return null;

        // 生成ID并设置有效期（先做本地缓存，后期放入redis中并设置有效时间）
        redisTemplate.opsForValue().set(loginImageId, WeixinLoginImageStateEnum.NOT_LOGGED, 3, TimeUnit.MINUTES);

        return WeixinUtil.getRedirectUrl(weixinConfig.getLoginImgeUrl() + loginImageId, weixinConfig.getAppid());
    }

    /**
     * 查询微信登录二维码状态
     *
     * @param loginImageId
     * @return
     */
    @Override
    public WeixinLoginImageStateEnum selectLoginImageUrlState(String loginImageId) {
        Object object = redisTemplate.opsForValue().get(loginImageId);
        if (object != null) {
            return (WeixinLoginImageStateEnum) object;
        } else {
            return null;
        }
    }

    /**
     * 根据微信用户临时授权code获取微信用户的openid
     *
     * @param code
     * @param loginImageId
     * @return
     */
    @Override
    public String getUserOpenid(String code, String loginImageId) {
        redisTemplate.opsForValue().set(loginImageId, WeixinLoginImageStateEnum.CODE_SCANNED, 3, TimeUnit.MINUTES);

        return WeixinUtil.getWeinxinOpenid(weixinConfig.getAppid(), code, weixinConfig.getAppsecret());
    }

    /**
     * 根据微信用户的openid获取用户信息
     *
     * @param openid
     * @param loginImageId
     * @return
     */
    @Override
    public SysUser getSysUserByOpenid(String openid, String loginImageId) {

        // 根据用户openid查询用户信息
        // 查询 openid对应的用户
        // Todo...
        SysUser sysUser = null;
        try {
            sysUser.setPassword(sysPasswordService.decryptPassword(sysUser.getLoginName(), sysUser.getPassword(), sysUser.getSalt()));
        } catch (Exception e) {
            log.error("用户密码解密失败" , e);
            sysUser = null;
        }
        if (sysUser != null)
            redisTemplate.opsForValue().set(loginImageId, WeixinLoginImageStateEnum.LOGIN_SUCCEEDED, 1, TimeUnit.MINUTES);
        else
            redisTemplate.delete(loginImageId);
        return sysUser;
    }

    /**
     * 根据微信用户临时授权code获取微信用户信息
     *
     * @param code
     * @param loginImageId
     * @return
     */
    @Override
    public SysUser getSysUserBycode(String code, String loginImageId) {
        return getSysUserByOpenid(getUserOpenid(code, loginImageId), loginImageId);
    }

    @Override
    public void setRedis(String loginImageId, SysUser sysUser) {
        redisTemplate.opsForValue().set(loginImageId + ".SysUser" , sysUser, 1, TimeUnit.MINUTES);
    }


    @Override
    public SysUser getRedis(String loginImageId) {
        return (SysUser) redisTemplate.opsForValue().get(loginImageId + ".SysUser" );
    }
}
