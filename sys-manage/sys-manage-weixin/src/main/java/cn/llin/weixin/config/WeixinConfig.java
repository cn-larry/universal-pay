package cn.llin.weixin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Larry
 * 2021/11/10 21:31
 * 微信相关配置
 *
 * @Version 1.0
 */
@Data
@Configuration
public class WeixinConfig {

    @Value("${spring.profiles.active}")
    private String active;

    /**
     * 微信应用APPID
     */
    @Value("${weixin.appid}" )
    private String appid;

    /**
     * 微信应用 appsecret
     */
    @Value("${weixin.appsecret}" )
    private String appsecret;

    /**
     * 微信登录二维码后台地址
     */
    @Value("${weixin.loginImgeUrl}" )
    private String loginImgeUrl;

    /**
     * 静态收款码地址
     */
    @Value("${weixin.qrCodeIndexUrl}" )
    private String qrCodeIndexUrl;

    /**
     * 小程序付款页面
     */
    @Value("${weixin.appletPayPath}" )
    private String appletPayPath;

}
