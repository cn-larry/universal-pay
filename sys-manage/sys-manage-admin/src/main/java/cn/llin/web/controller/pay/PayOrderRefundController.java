package cn.llin.web.controller.pay;

import cn.llin.common.annotation.Log;
import cn.llin.common.core.controller.BaseController;
import cn.llin.common.core.domain.AjaxResult;
import cn.llin.common.core.page.TableDataInfo;
import cn.llin.common.enums.BusinessType;
import cn.llin.common.utils.poi.ExcelUtil;
import cn.llin.pay.domain.PayOrderRefund;
import cn.llin.pay.service.IPayOrderRefundService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 退款Controller
 *
 * @author Larry
 * @date 2021-11-10
 */
@Api(tags = "退款Controller" )
@Controller
@RequestMapping("/pay/refund" )
public class PayOrderRefundController extends BaseController {

    private String prefix = "pay/refund" ;

    @Autowired
    private IPayOrderRefundService payOrderRefundService;

    @RequiresPermissions("pay:refund:view" )
    @GetMapping()
    public String refund() {
        return prefix + "/refund" ;
    }

    /**
     * 查询退款列表
     */
    @RequiresPermissions("pay:refund:list" )
    @PostMapping("/list" )
    @ResponseBody
    public TableDataInfo list(PayOrderRefund payOrderRefund) {
        startPage();
        List<PayOrderRefund> list = payOrderRefundService.selectPayOrderRefundList(payOrderRefund);
        return getDataTable(list);
    }

    /**
     * 导出退款列表
     */
    @RequiresPermissions("pay:refund:export" )
    @Log(title = "退款" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    @ResponseBody
    public AjaxResult export(PayOrderRefund payOrderRefund) {
        List<PayOrderRefund> list = payOrderRefundService.selectPayOrderRefundList(payOrderRefund);
        ExcelUtil<PayOrderRefund> util = new ExcelUtil<PayOrderRefund>(PayOrderRefund.class);
        return util.exportExcel(list, "退款数据" );
    }

    /**
     * 新增退款
     */
    @GetMapping("/add" )
    public String add() {
        return prefix + "/add" ;
    }

    /**
     * 新增保存退款
     */
    @RequiresPermissions("pay:refund:add" )
    @Log(title = "退款" , businessType = BusinessType.INSERT)
    @PostMapping("/add" )
    @ResponseBody
    public AjaxResult addSave(PayOrderRefund payOrderRefund) {
        return toAjax(payOrderRefundService.insertPayOrderRefund(payOrderRefund));
    }

    /**
     * 修改退款
     */
    @GetMapping("/edit/{id}" )
    public String edit(@PathVariable("id" ) Long id, ModelMap mmap) {
        PayOrderRefund payOrderRefund = payOrderRefundService.selectPayOrderRefundById(id);
        mmap.put("payOrderRefund" , payOrderRefund);
        return prefix + "/edit" ;
    }

    /**
     * 修改保存退款
     */
    @RequiresPermissions("pay:refund:edit" )
    @Log(title = "退款" , businessType = BusinessType.UPDATE)
    @PostMapping("/edit" )
    @ResponseBody
    public AjaxResult editSave(PayOrderRefund payOrderRefund) {
        return toAjax(payOrderRefundService.updatePayOrderRefund(payOrderRefund));
    }

    /**
     * 删除退款
     */
    @RequiresPermissions("pay:refund:remove" )
    @Log(title = "退款" , businessType = BusinessType.DELETE)
    @PostMapping("/remove" )
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(payOrderRefundService.deletePayOrderRefundByIds(ids));
    }
}
