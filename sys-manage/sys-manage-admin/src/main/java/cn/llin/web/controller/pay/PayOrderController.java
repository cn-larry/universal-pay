package cn.llin.web.controller.pay;

import cn.llin.common.annotation.Log;
import cn.llin.common.core.controller.BaseController;
import cn.llin.common.core.domain.AjaxResult;
import cn.llin.common.core.page.TableDataInfo;
import cn.llin.common.enums.BusinessType;
import cn.llin.common.utils.poi.ExcelUtil;
import cn.llin.pay.domain.PayOrder;
import cn.llin.pay.service.IPayOrderService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 交易记录Controller
 *
 * @author larry
 * @date 2021-11-10
 */
@Api(tags = "交易记录Controller" )
@Controller
@RequestMapping("/pay/order" )
public class PayOrderController extends BaseController {

    private String prefix = "pay/order" ;

    @Autowired
    private IPayOrderService payOrderService;

    @RequiresPermissions("pay:order:view" )
    @GetMapping()
    public String order() {
        return prefix + "/order" ;
    }

    /**
     * 查询交易记录列表
     */
    @RequiresPermissions("pay:order:list" )
    @PostMapping("/list" )
    @ResponseBody
    public TableDataInfo list(PayOrder payOrder) {
        startPage();
        List<PayOrder> list = payOrderService.selectPayOrderList(payOrder);
        return getDataTable(list);
    }

    /**
     * 导出交易记录列表
     */
    @RequiresPermissions("pay:order:export" )
    @Log(title = "交易记录" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    @ResponseBody
    public AjaxResult export(PayOrder payOrder) {
        List<PayOrder> list = payOrderService.selectPayOrderList(payOrder);
        ExcelUtil<PayOrder> util = new ExcelUtil<PayOrder>(PayOrder.class);
        return util.exportExcel(list, "交易记录数据" );
    }

    /**
     * 新增交易记录
     */
    @GetMapping("/add" )
    public String add() {
        return prefix + "/add" ;
    }

    /**
     * 新增保存交易记录
     */
    @RequiresPermissions("pay:order:add" )
    @Log(title = "交易记录" , businessType = BusinessType.INSERT)
    @PostMapping("/add" )
    @ResponseBody
    public AjaxResult addSave(PayOrder payOrder) {
        return toAjax(payOrderService.insertPayOrder(payOrder));
    }

    /**
     * 修改交易记录
     */
    @GetMapping("/edit/{id}" )
    public String edit(@PathVariable("id" ) Long id, ModelMap mmap) {
        PayOrder payOrder = payOrderService.selectPayOrderById(id);
        mmap.put("payOrder" , payOrder);
        return prefix + "/edit" ;
    }

    /**
     * 修改保存交易记录
     */
    @RequiresPermissions("pay:order:edit" )
    @Log(title = "交易记录" , businessType = BusinessType.UPDATE)
    @PostMapping("/edit" )
    @ResponseBody
    public AjaxResult editSave(PayOrder payOrder) {
        return toAjax(payOrderService.updatePayOrder(payOrder));
    }

    /**
     * 删除交易记录
     */
    @RequiresPermissions("pay:order:remove" )
    @Log(title = "交易记录" , businessType = BusinessType.DELETE)
    @PostMapping("/remove" )
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(payOrderService.deletePayOrderByIds(ids));
    }
}
