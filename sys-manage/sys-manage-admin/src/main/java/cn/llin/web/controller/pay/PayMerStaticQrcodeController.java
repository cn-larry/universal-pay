package cn.llin.web.controller.pay;

import cn.llin.common.annotation.Log;
import cn.llin.common.core.controller.BaseController;
import cn.llin.common.core.domain.AjaxResult;
import cn.llin.common.core.page.TableDataInfo;
import cn.llin.common.enums.BusinessType;
import cn.llin.common.utils.poi.ExcelUtil;
import cn.llin.pay.domain.PayMerStaticQrcode;
import cn.llin.pay.exceptions.SysException;
import cn.llin.pay.service.IPayMerStaticQrcodeService;
import cn.llin.pay.service.IQrCodePayService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户静态收款码Controller
 *
 * @author larry
 * @date 2021-11-10
 */
@Api(tags = "商户静态收款码Controller" )
@Controller
@RequestMapping("/pay/qrcode" )
public class PayMerStaticQrcodeController extends BaseController {

    private String prefix = "pay/qrcode" ;

    @Autowired
    private IPayMerStaticQrcodeService payMerStaticQrcodeService;
    @Autowired
    IQrCodePayService qrCodePayService;

    @RequiresPermissions("pay:qrcode:view" )
    @GetMapping()
    public String qrcode() {
        return prefix + "/qrcode" ;
    }

    /**
     * 查询商户静态收款码列表
     */
    @RequiresPermissions("pay:qrcode:list" )
    @PostMapping("/list" )
    @ResponseBody
    public TableDataInfo list(PayMerStaticQrcode payMerStaticQrcode) {
        startPage();
        List<PayMerStaticQrcode> list = payMerStaticQrcodeService.selectPayMerStaticQrcodeList(payMerStaticQrcode);
        return getDataTable(list);
    }

    /**
     * 导出商户静态收款码列表
     */
    @RequiresPermissions("pay:qrcode:export" )
    @Log(title = "商户静态收款码" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    @ResponseBody
    public AjaxResult export(PayMerStaticQrcode payMerStaticQrcode) {
        List<PayMerStaticQrcode> list = payMerStaticQrcodeService.selectPayMerStaticQrcodeList(payMerStaticQrcode);
        ExcelUtil<PayMerStaticQrcode> util = new ExcelUtil<PayMerStaticQrcode>(PayMerStaticQrcode.class);
        return util.exportExcel(list, "商户静态收款码数据" );
    }

    /**
     * 新增商户静态收款码
     */
    @GetMapping("/add" )
    public String add() {
        return prefix + "/add" ;
    }

    /**
     * 新增保存商户静态收款码
     */
    @RequiresPermissions("pay:qrcode:add" )
    @Log(title = "商户静态收款码" , businessType = BusinessType.INSERT)
    @PostMapping("/add" )
    @ResponseBody
    public AjaxResult addSave(PayMerStaticQrcode payMerStaticQrcode) {
        return toAjax(payMerStaticQrcodeService.insertPayMerStaticQrcode(payMerStaticQrcode));
    }

    /**
     * 修改商户静态收款码
     */
    @GetMapping("/edit/{qrcodeId}" )
    public String edit(@PathVariable("qrcodeId" ) String qrcodeId, ModelMap mmap) {
        PayMerStaticQrcode payMerStaticQrcode = payMerStaticQrcodeService.selectPayMerStaticQrcodeByQrcodeId(qrcodeId);
        mmap.put("payMerStaticQrcode" , payMerStaticQrcode);
        return prefix + "/edit" ;
    }

    /**
     * 修改保存商户静态收款码
     */
    @RequiresPermissions("pay:qrcode:edit" )
    @Log(title = "商户静态收款码" , businessType = BusinessType.UPDATE)
    @PostMapping("/edit" )
    @ResponseBody
    public AjaxResult editSave(PayMerStaticQrcode payMerStaticQrcode) {
        return toAjax(payMerStaticQrcodeService.updatePayMerStaticQrcode(payMerStaticQrcode));
    }

    /**
     * 删除商户静态收款码
     */
    @RequiresPermissions("pay:qrcode:remove" )
    @Log(title = "商户静态收款码" , businessType = BusinessType.DELETE)
    @PostMapping("/remove" )
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(payMerStaticQrcodeService.deletePayMerStaticQrcodeByQrcodeIds(ids));
    }

    /**
     * 获取临时的URL，仅可作为演示
     */
    @RequiresPermissions("pay:qrcode:temporaryUrl" )
    @Log(title = "商户静态收款码" , businessType = BusinessType.DELETE)
    @PostMapping("/temporaryUrl" )
    @ResponseBody
    public AjaxResult temporaryUrl(@PathVariable("qrcodeId" ) String qrcodeId) {
        try {
            return success(qrCodePayService.getWeixinAppletPayUrl(qrcodeId, 10));
        } catch (SysException e) {
            return error(e.getMessage());
        }
    }
}
