package cn.llin.web.controller.pay.api;

import cn.llin.common.core.controller.BaseController;
import cn.llin.common.core.domain.AjaxResult;
import cn.llin.pay.beans.QrCodeBean;
import cn.llin.pay.enums.PayTypeEnum;
import cn.llin.pay.exceptions.SysException;
import cn.llin.pay.service.IGetPayUseridService;
import cn.llin.pay.service.IPayMerService;
import cn.llin.pay.service.IQrCodePayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * Larry
 * 2021/11/10 19:35
 * 收款码Controller
 *
 * @Version 1.0
 */
@Slf4j
@Api(tags = "收款码Controller")
@Controller
@RequestMapping("/qrCode")
public class QrCodeController extends BaseController {

    @Autowired
    IGetPayUseridService getPayUseridService;
    @Autowired
    IPayMerService payMerService;
    @Autowired
    IQrCodePayService qrCodePayService;

    /**
     * 查询商户信息
     *
     * @param merId
     * @return
     */
    @ApiOperation("查询商户信息")
    @PostMapping("/getMerInfo")
    @ResponseBody
    public AjaxResult getMerInfo(String merId) {
        try {
            return success(payMerService.selectPayMerByMerId(Long.valueOf(merId)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }

    @ApiOperation("获取支付宝小程序二维码")
    @PostMapping("/getAlipayAppletPayUrl")
    @ResponseBody
    public AjaxResult getAlipayAppletPayUrl(String merId) {
        try {
            if (StringUtils.isBlank(merId))
                return error("merId not null");

            return success(qrCodePayService.getAlipayAppletUrl(merId));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }


    /**
     * 获取微信小程序支付二维码，需手输金额的
     * （防止永久URL过多，暂定为有效期10天；该接口适用于分享支付）
     * 适用场景：非微信端支付场景，支持微信支付
     *
     * @param merId
     * @return
     */
    @ApiOperation("获取微信小程序支付二维码")
    @PostMapping("/getWeixinAppletPayUrlBy10Day")
    @ResponseBody
    public AjaxResult getWeixinAppletPayUrlBy10Day(String merId) {
        try {
            if (StringUtils.isBlank(merId))
                return error("merId not null");

            return success(qrCodePayService.getWeixinAppletPayUrl(merId, 10));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }

    /**
     * 微信用户code换取openid
     *
     * @param reqBean
     * @return
     */
    @ApiOperation("微信用户code换取openid")
    @PostMapping("/weixin/getOpenid")
    @ResponseBody
    public AjaxResult getOpenid(QrCodeBean reqBean) {
        try {
            // 拦截非微信浏览器
            interceptWeixinBrowser();
            if (!PayTypeEnum.WEIXIN.equals(reqBean.getPayType()))
                error("Your payment method does not support");

            return success(getPayUseridService.getWeinxinOpenid(reqBean.getCode()));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }


    /**
     * 收款二维码入口（接口的'01'暂定为：静态收款码）
     * 该接口可用于微信公众号支付模式，但是满足不了非微信应用下的支付（接口暂为弃用）
     *
     * @param qrcodeId
     * @param request
     * @param response
     * @throws Exception
     */
//    @ApiOperation("收款二维码入口")
//    @RequestMapping(value = "/01/{qrcodeId}" )
    @Deprecated
    public void qrcodeId01(@NotNull @PathVariable String qrcodeId,
                           HttpServletRequest request,
                           HttpServletResponse response) throws Exception {
        if (judgeWeixinBrowser()) {// 判断微信访问
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write("Please use Wechat to open");
            return;
        }

        try {
            redirect(getPayUseridService.getReqWeixinUrl(qrcodeId));
        } catch (SysException e) {
            log.error(e.getMessage(), e);
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write(e.getMessage());
        }
    }

}
