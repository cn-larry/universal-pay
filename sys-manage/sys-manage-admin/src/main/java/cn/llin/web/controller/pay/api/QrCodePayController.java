package cn.llin.web.controller.pay.api;

import cn.llin.common.core.controller.BaseController;
import cn.llin.common.core.domain.AjaxResult;
import cn.llin.pay.beans.QrCodePayCreateBean;
import cn.llin.pay.beans.QrCodePayQueryBean;
import cn.llin.pay.enums.PayTypeEnum;
import cn.llin.pay.service.IQrCodePayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Larry
 * 2021/11/11 21:30
 * 收款交易相关Controller
 *
 * @Version 1.0
 */
@Slf4j
@Api(tags = "收款交易相关Controller")
@Controller
@RequestMapping("/qrCodePay")
public class QrCodePayController extends BaseController {

    @Autowired
    IQrCodePayService qrCodePayService;

    /**
     * 创建交易链接
     *
     * @param reqBean
     * @return
     */
    @ApiOperation(value = "（小程序）创建交易链接", notes = "（适用于外部订单创建后跳转至小程序内付款）")
    @PostMapping("/createAppletUrl")
    @ResponseBody
    public AjaxResult createAppletUrl(QrCodePayCreateBean reqBean) {
        try {
            return success(qrCodePayService.createPayUrl(reqBean));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }

    /**
     * 创建交易
     *
     * @param payNo
     * @return
     */
    @ApiOperation(value = "（小程序）创建交易", notes = "（适用于小程序打开后，小程序内付款）")
    @PostMapping("/appletCreate")
    @ResponseBody
    public AjaxResult appletCreate(String payNo) {
        try {
            return success(qrCodePayService.create(payNo));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }

    /**
     * 交易创建
     *
     * @param reqBean
     * @return
     */
    @ApiOperation("交易创建")
    @PostMapping("/create")
    @ResponseBody
    public AjaxResult create(QrCodePayCreateBean reqBean) {
        try {
            interceptWeixinBrowser();
            if (!PayTypeEnum.WEIXIN.equals(reqBean.getPayType()))
                error("Your payment method does not support");

            return success(qrCodePayService.create(reqBean));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }

    /**
     * 交易查询
     *
     * @param reqBean
     * @return
     */
    @ApiOperation("交易查询")
    @PostMapping("/query")
    @ResponseBody
    public AjaxResult query(QrCodePayQueryBean reqBean) {
        try {
            return success(qrCodePayService.query(reqBean));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return error(e.getMessage());
        }
    }

}
