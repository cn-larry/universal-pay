package cn.llin.web.controller.pay.api;

import cn.llin.pay.beans.QrCodePayNotifyBean;
import cn.llin.pay.enums.PayTypeEnum;
import cn.llin.pay.service.IQrCodePayService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URLDecoder;
import java.util.Date;

/**
 * Larry
 * 2021/11/10 21:01
 * 交易异步通知Controller
 *
 * @Version 1.0
 */
@Slf4j
@Api(tags = "交易异步通知Controller" )
@Controller
@RequestMapping("/payNotify" )
public class PayNotifyController {

    @Autowired
    IQrCodePayService qrCodePayService;

    /**
     * 交易异步通知回调接口
     * >>后期需要针对不同的交易类型，编写不同的回调地址（支付渠道交易通知报文可以不一样）
     *
     * @param requestStr
     * @return
     */
    @ApiOperation("交易异步通知回调接口")
    @ResponseBody
    @RequestMapping(value = "/weixin" )
    public String weixin(@RequestBody String requestStr) {

        log.info("微信交易通知报文：{}" , requestStr);

        String respStr = "fail" ;

        try {
            String reqStr = URLDecoder.decode(requestStr, "UTF-8" );
            if (StringUtils.isNotBlank(reqStr)) {
                JSONObject jsonObject = JSONObject.parseObject(reqStr.substring(0, reqStr.length() - 1));

                // 可拦截交易不成功的交易（退款等其他非正向交易异步通知，建议通知地址分开）
                // Todo...

                QrCodePayNotifyBean bean = new QrCodePayNotifyBean();
                bean.setPayNo(jsonObject.getString("payNo" ));
                bean.setPayAmt((Long) jsonObject.get("payAmt" ));
                bean.setPayType(PayTypeEnum.WEIXIN);
                bean.setPayUserId(jsonObject.getString("payUserId" ));
                bean.setPayTime(new Date());
                bean.setChannelPayNo(jsonObject.getString("channelPayNo" ));
                bean.setChannelMerNo(jsonObject.getString("channelMerNo" ));
                if (qrCodePayService.successNotify(bean))
                    respStr = "ok" ;
            }
        } catch (Exception e) {
            log.error("处理交易通知异常" , e);
        }
        log.info("微信交易通知应答：{}" , respStr);
        return respStr;
    }
}
