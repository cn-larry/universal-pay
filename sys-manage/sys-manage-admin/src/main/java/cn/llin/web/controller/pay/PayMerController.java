package cn.llin.web.controller.pay;

import cn.llin.common.annotation.Log;
import cn.llin.common.core.controller.BaseController;
import cn.llin.common.core.domain.AjaxResult;
import cn.llin.common.core.page.TableDataInfo;
import cn.llin.common.enums.BusinessType;
import cn.llin.common.utils.poi.ExcelUtil;
import cn.llin.pay.domain.PayMer;
import cn.llin.pay.service.IPayMerService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户Controller
 *
 * @author larry
 * @date 2021-11-10
 */
@Api(tags = "商户管理Controller" )
@Controller
@RequestMapping("/pay/merInfo" )
public class PayMerController extends BaseController {

    private String prefix = "pay/merInfo" ;

    @Autowired
    private IPayMerService payMerService;

    @RequiresPermissions("pay:merInfo:view" )
    @GetMapping()
    public String merInfo() {
        return prefix + "/merInfo" ;
    }

    /**
     * 查询商户列表
     */
    @RequiresPermissions("pay:merInfo:list" )
    @PostMapping("/list" )
    @ResponseBody
    public TableDataInfo list(PayMer payMer) {
        startPage();
        List<PayMer> list = payMerService.selectPayMerList(payMer);
        return getDataTable(list);
    }

    /**
     * 导出商户列表
     */
    @RequiresPermissions("pay:merInfo:export" )
    @Log(title = "商户" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    @ResponseBody
    public AjaxResult export(PayMer payMer) {
        List<PayMer> list = payMerService.selectPayMerList(payMer);
        ExcelUtil<PayMer> util = new ExcelUtil<PayMer>(PayMer.class);
        return util.exportExcel(list, "商户数据" );
    }

    /**
     * 新增商户
     */
    @GetMapping("/add" )
    public String add() {
        return prefix + "/add" ;
    }

    /**
     * 新增保存商户
     */
    @RequiresPermissions("pay:merInfo:add" )
    @Log(title = "商户" , businessType = BusinessType.INSERT)
    @PostMapping("/add" )
    @ResponseBody
    public AjaxResult addSave(PayMer payMer) {
        return toAjax(payMerService.insertPayMer(payMer));
    }

    /**
     * 修改商户
     */
    @GetMapping("/edit/{merId}" )
    public String edit(@PathVariable("merId" ) Long merId, ModelMap mmap) {
        PayMer payMer = payMerService.selectPayMerByMerId(merId);
        mmap.put("payMer" , payMer);
        return prefix + "/edit" ;
    }

    /**
     * 修改保存商户
     */
    @RequiresPermissions("pay:merInfo:edit" )
    @Log(title = "商户" , businessType = BusinessType.UPDATE)
    @PostMapping("/edit" )
    @ResponseBody
    public AjaxResult editSave(PayMer payMer) {
        return toAjax(payMerService.updatePayMer(payMer));
    }

    /**
     * 删除商户
     */
    @RequiresPermissions("pay:merInfo:remove" )
    @Log(title = "商户" , businessType = BusinessType.DELETE)
    @PostMapping("/remove" )
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(payMerService.deletePayMerByMerIds(ids));
    }
}
