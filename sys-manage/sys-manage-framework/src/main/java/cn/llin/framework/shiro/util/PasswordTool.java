package cn.llin.framework.shiro.util;

/**
 * Larry
 * 2021/10/14 13:53
 *
 * @Version 1.0
 */
public class PasswordTool {

    private static final String SECURITY_KEY = "122e8-=234.8937*23djnqd23we";


    /**
     * AES 加密
     * @param password
     *         未加密的密码
     * @param salt
     *         盐值，默认使用用户名就可
     * @return
     * @throws Exception
     */
    public static String encrypt(String password, String salt) {
        return AesTool.encrypt(Md5Tool.MD5(salt + SECURITY_KEY), password);
    }

    /**
     * AES 解密
     * @param encryptPassword
     *         加密后的密码
     * @param salt
     *         盐值，默认使用用户名就可
     * @return
     * @throws Exception
     */
    public static String decrypt(String encryptPassword, String salt) throws Exception {
        return AesTool.decrypt(Md5Tool.MD5(salt + SECURITY_KEY), encryptPassword);
    }

}
