package cn.llin.pay.exceptions;

import lombok.Data;

/**
 * Larry
 * 2021/11/13 20:01
 *
 * @Version 1.0
 */
@Data
public class SysException extends Exception {

    private String sysCode;
    private String sysCodeMsg;

//    public SysException(){
//        super();
//    }

    public SysException(String exceptionMsg) {
        this.sysCodeMsg = exceptionMsg;
    }

    public SysException(String sysCode, String codeMsg) {
        this.sysCode = sysCode;
        this.sysCodeMsg = codeMsg;
    }

    public SysException(Throwable e) {
        if (e instanceof SysException) {
            this.sysCode = ((SysException) e).getSysCode();
            this.sysCodeMsg = ((SysException) e).getSysCodeMsg();
        } else {
        }
//        initCause(e);
    }

    public String getMessage() {
        if (getCause() != null)
            return getCause().getMessage();
        return this.sysCodeMsg;
    }

    public void printStackTrace() {
        if (getCause() != null)
            getCause().printStackTrace();
        else
            super.printStackTrace();
    }

}
