package cn.llin.pay.service.impl;

import cn.llin.pay.domain.PayMer;
import cn.llin.pay.domain.PayMerStaticQrcode;
import cn.llin.pay.enums.MerStateEnum;
import cn.llin.pay.exceptions.SysException;
import cn.llin.pay.service.IGetPayUseridService;
import cn.llin.pay.service.IPayMerService;
import cn.llin.pay.service.IPayMerStaticQrcodeService;
import cn.llin.weixin.config.WeixinConfig;
import cn.llin.weixin.utils.WeixinUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * Larry
 * 2021/11/10 20:29
 *
 * @Version 1.0
 */
@Slf4j
@Service
public class GetPayUseridServiceImpl implements IGetPayUseridService {

    @Autowired
    IPayMerStaticQrcodeService payMerStaticQrcodeService;
    @Autowired
    IPayMerService payMerService;
    @Autowired
    WeixinConfig weixinConfig;

    /**
     * 根据二维码ID拼接请求微信URL（获取用户授权Code的URL）
     *
     * @param qrCodeId
     * @return
     * @throws Exception
     */
    @Override
    public String getReqWeixinUrl(@NotNull String qrCodeId) throws SysException {
        if (StringUtils.isBlank(qrCodeId))
            throw new SysException("qrCodeId is null" );

        // 查询二维码信息
        PayMerStaticQrcode payMerStaticQrcode = payMerStaticQrcodeService.selectPayMerStaticQrcodeByQrcodeId(qrCodeId);
        if (payMerStaticQrcode == null)
            throw new SysException("qrCodeId can't find" );
        if (!"1".equals(payMerStaticQrcode.getQrcodeStatus()))
            throw new SysException("qrCodeId is invalid" );

        // 查询商户信息
        PayMer payMer = payMerService.selectPayMerByMerId(payMerStaticQrcode.getMerId());
        if (payMer == null)
            throw new SysException("merchant can't find" );
        if (!MerStateEnum.NORMAL_BUSINESS.name().equals(payMer.getMerStatus()))
            throw new SysException("merchant status is " + payMer.getMerStatus());

        String qrCodeIndexUrl = weixinConfig.getQrCodeIndexUrl();
        qrCodeIndexUrl += "?qrCodeId=" + qrCodeId
                + "&merId=" + payMer.getMerId()
                + "&merName=" + payMer.getMerName();
//                + "&merSubName=" + payMer.getMerSubName();
        return WeixinUtil.getRedirectUrl(qrCodeIndexUrl, weixinConfig.getAppid());

    }

    /**
     * 使用微信用户临时授权的code获取用户openid
     *
     * @param code
     * @return
     * @throws Exception
     */
    @Override
    public String getWeinxinOpenid(@NotNull String code) throws SysException {
        if (StringUtils.isBlank(code))
            throw new SysException("code is null" );

        if("dev".equals(weixinConfig.getActive()))// 测试使用
            return "oD1bO5QeX_b-7qCjLOam3TafzhBo";

        return WeixinUtil.getWeinxinOpenid(weixinConfig.getAppid(), code, weixinConfig.getAppsecret());
    }

}
