package cn.llin.pay.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.llin.pay.beans.QrCodePayCreateBean;
import cn.llin.pay.beans.QrCodePayNotifyBean;
import cn.llin.pay.beans.QrCodePayQueryBean;
import cn.llin.pay.config.AlipayConfig;
import cn.llin.pay.domain.PayOrder;
import cn.llin.pay.enums.PayStateEnum;
import cn.llin.pay.enums.PayTypeEnum;
import cn.llin.pay.exceptions.SysException;
import cn.llin.pay.mapper.PayOrderMapper;
import cn.llin.pay.service.IPayMerService;
import cn.llin.pay.service.IQrCodePayService;
import cn.llin.pay.utils.AppletUrlUtils;
import cn.llin.pay.utils.PayUtils;
import cn.llin.pay.utils.RedisUtils;
import cn.llin.weixin.config.WeixinConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Larry
 * 2021/11/10 20:29 @Version 1.0
 * 2021/11/16 14:29 @Version 1.1
 */
@Slf4j
@Service
public class QrCodePayServiceImpl implements IQrCodePayService {

    @Autowired
    PayOrderMapper payOrderMapper;
    @Autowired
    WeixinConfig weixinConfig;
    @Autowired
    AlipayConfig alipayConfig;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    IPayMerService payMerService;

    /**
     * 创建交易链接（适用于外部订单创建后跳转至小程序内付款）
     *
     * @param bean
     * @return
     * @throws SysException
     */
    @Override
    public String createPayUrl(QrCodePayCreateBean bean) throws SysException {

        String payNo = PayUtils.getPayNo(bean.getPayType());
        bean.setPayNo(payNo);
        bean.setOrderTime(new Date());
        PayOrder payOrder = new PayOrder();
        BeanUtil.copyProperties(bean, payOrder);

        redisUtils.setPayNo(payNo, payOrder);
        log.info("payNo：{}", payNo);
        String queryParam = "payNo=" + payNo;
        if (bean.getMerId() != null && bean.getMerId() > 0)
            queryParam += "&merId=" + bean.getMerId();

        // 获取小程序链接
        return getAppletUrl(bean.getPayType(), bean.getMerId() + "", 0, queryParam);
    }

    /**
     * 交易创建（适用于'创建交易链接'后小程序内请求）
     *
     * @param payNo
     * @return
     * @throws SysException
     */
    @Override
    public QrCodePayCreateBean create(String payNo) throws SysException {

        PayOrder payOrder = redisUtils.getPayNo(payNo);

        QrCodePayCreateBean bean = new QrCodePayCreateBean();
        BeanUtil.copyProperties(payOrder, bean);
        bean.setPrepayId(create(payOrder));

        bean.setPayMer(payMerService.selectPayMerByMerId(payOrder.getMerId()));
        return bean;
    }

    /**
     * （收款二维码）交易创建
     *
     * @param bean
     * @return
     * @throws SysException
     */
    @Override
    public QrCodePayCreateBean create(QrCodePayCreateBean bean) throws SysException {

        bean.setPayNo(PayUtils.getPayNo());
        bean.setOrderTime(new Date());
        PayOrder payOrder = new PayOrder();
        BeanUtil.copyProperties(bean, payOrder);

        bean.setPrepayId(create(payOrder));
        return bean;
    }

    /**
     * 创建订单入库，并获取'预支付交易会话标识'
     *
     * @param payOrder
     * @return
     * @throws SysException
     */
    private String create(PayOrder payOrder) throws SysException {

        try {
            payOrderMapper.insertPayOrder(payOrder);
        } catch (Exception e) {
            if(e instanceof DuplicateKeyException)
                throw new SysException("payOrder is repeat request");
            else{
                log.error("创建订单入库异常", e);
                throw new SysException(e.getMessage());
            }
        }

        // 本应该请求渠道获取'预支付交易会话标识'，现模拟数据
        return PayUtils.getPrepayId(PayTypeEnum.valueOf(payOrder.getPayType()));
    }

    /**
     * 交易查询
     *
     * @param bean
     * @return
     * @throws SysException
     */
    @Override
    public QrCodePayQueryBean query(QrCodePayQueryBean bean) throws SysException {

        PayOrder payOrder = payOrderMapper.selectPayOrderByPayNo(bean.getPayNo());
        if (payOrder == null)
            throw new SysException("No related records were found");

        BeanUtil.copyProperties(payOrder, bean);

        // 查询商户信息
        bean.setPayMer(payMerService.selectPayMerByMerId(payOrder.getMerId()));

        return bean;
    }

    /**
     * 交易成功（异步）通知
     *
     * @param bean
     * @return
     * @throws SysException
     */
    @Override
    public Boolean successNotify(QrCodePayNotifyBean bean) throws SysException {

        PayOrder payOrder = payOrderMapper.selectPayOrderByPayNo(bean.getPayNo());
        if (payOrder == null)
            return false;

        // 判断库表中交易订单状态（只有待支付才更新数据库）
        if (PayStateEnum.NOT_PAY.name().equals(payOrder.getPayStatus())) {
            // 更新
            BeanUtil.copyProperties(bean, payOrder, CopyOptions.create().setIgnoreNullValue(true));
            payOrderMapper.updatePayOrderAndPayStateIsNOT_PAY(payOrder);
        } else if (PayStateEnum.FAIL.name().equals(payOrder.getPayStatus())) {
            return false;
        }

        return true;
    }

    /**
     * 获取微信小程序UrlLink
     *
     * @param merId
     * @param expireIntervalDay 有效天数
     * @return
     * @throws SysException
     */
    @Override
    public String getWeixinAppletPayUrl(String merId, int expireIntervalDay) throws SysException {
        return AppletUrlUtils.getWeixinAppletUrl(weixinConfig.getAppid(),
                weixinConfig.getAppsecret(),
                weixinConfig.getAppletPayPath(),
                "merId=" + merId,
                redisTemplate,
                expireIntervalDay);
    }

    /**
     * 获取支付宝小程序二维码
     * （小程序跳转参考：https://opendocs.alipay.com/mini/api/xqvxl4）
     *
     * @param merId
     * @return
     * @throws SysException
     */
    @Override
    public String getAlipayAppletUrl(String merId) throws SysException {
        return AppletUrlUtils.getAlipayAppletUrl(alipayConfig.getAppid(),
                alipayConfig.getAppPrivateKey(),
                alipayConfig.getAlipayPublicKey(),
                alipayConfig.getAppletPayPath(),
                "merId=" + merId,
                "支付宝支付");
    }

    /**
     * 获取小程序链接
     *
     * @param payType           参考"PayTypeEnum.java"
     * @param merId
     * @param expireIntervalDay 只针对微信小程序有效（建议不超过10天）
     * @param queryParam
     * @return
     * @throws SysException
     */
    @Override
    public String getAppletUrl(PayTypeEnum payType, String merId, int expireIntervalDay, String queryParam) throws SysException {

        if (PayTypeEnum.WEIXIN.equals(payType))
            return AppletUrlUtils.getWeixinAppletUrl(weixinConfig.getAppid(),
                    weixinConfig.getAppsecret(),
                    weixinConfig.getAppletPayPath(),
                    queryParam,
                    redisTemplate,
                    expireIntervalDay);
        else if (PayTypeEnum.ALIPAY.equals(payType))
            return AppletUrlUtils.getAlipayAppletUrl(alipayConfig.getAppid(),
                    alipayConfig.getAppPrivateKey(),
                    alipayConfig.getAlipayPublicKey(),
                    alipayConfig.getAppletPayPath(),
                    queryParam,
                    "支付宝支付");
        else
            throw new SysException("This payType is not supported");
    }

}
