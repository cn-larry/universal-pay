package cn.llin.pay.service;

import cn.llin.pay.domain.PayMer;

import java.util.List;

/**
 * 商户Service接口
 * 
 * @author larry
 * @date 2021-11-10
 */
public interface IPayMerService {
    /**
     * 查询商户
     * 
     * @param merId 商户主键
     * @return 商户
     */
    public PayMer selectPayMerByMerId(Long merId);

    /**
     * 查询商户列表
     * 
     * @param payMer 商户
     * @return 商户集合
     */
    public List<PayMer> selectPayMerList(PayMer payMer);

    /**
     * 新增商户
     * 
     * @param payMer 商户
     * @return 结果
     */
    public int insertPayMer(PayMer payMer);

    /**
     * 修改商户
     * 
     * @param payMer 商户
     * @return 结果
     */
    public int updatePayMer(PayMer payMer);

    /**
     * 批量删除商户
     * 
     * @param merIds 需要删除的商户主键集合
     * @return 结果
     */
    public int deletePayMerByMerIds(String merIds);

    /**
     * 删除商户信息
     * 
     * @param merId 商户主键
     * @return 结果
     */
    public int deletePayMerByMerId(Long merId);
}
