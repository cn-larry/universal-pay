package cn.llin.pay.utils;

import cn.com.llin.time.TimeTool;
import cn.llin.pay.enums.PayTypeEnum;
import com.alibaba.fastjson.JSONObject;

/**
 * Larry
 * 2021/11/10 21:43
 *
 * @Version 1.0
 */
public class PayUtils {

    /**
     * 生成订单号
     *
     * @return
     */
    public static String getPayNo() {
        return TimeTool.getNowTime(null) + System.currentTimeMillis();
    }

    /**
     * 生成订单号
     *
     * @param typeEnum
     * @return
     */
    public static String getPayNo(PayTypeEnum typeEnum) {
        String typeCode = "00";

        if (PayTypeEnum.WEIXIN.equals(typeEnum))
            typeCode = "10";
        else if (PayTypeEnum.ALIPAY.equals(typeEnum))
            typeCode = "20";
        else if (PayTypeEnum.UNIONPAY.equals(typeEnum))
            typeCode = "30";

        return TimeTool.getNowTime(null) + typeCode + System.currentTimeMillis();
    }

    /**
     * 模拟生成支付预标识（测试使用）
     *
     * @param typeEnum
     * @return
     */
    public static String getPrepayId(PayTypeEnum typeEnum) {

        if (PayTypeEnum.WEIXIN.equals(typeEnum))
            return getWeixinPrepayId();
        else if (PayTypeEnum.ALIPAY.equals(typeEnum))
            return "20180001" + System.currentTimeMillis();
        else
            return "";
    }

    /**
     * 模拟'预支付交易会话标识'返回
     * 具体内容参考：https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=7_7&index=6tradetype
     *
     * @return
     */
    public static String getWeixinPrepayId() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("appId", "wx8888888888888888");
        jsonObject.put("timeStamp", "1414561699");
        jsonObject.put("nonceStr", "5K8264ILTKCH16CQ2502SI8ZNMTM67VS");
        jsonObject.put("package", "prepay_id=123456789");
        jsonObject.put("signType", "MD5");
        jsonObject.put("paySign", "C380BEC2BFD727A4B6845133519F3AD6");
        return jsonObject.toJSONString();
    }

    public static void main(String[] args) {
        System.out.println(getWeixinPrepayId());
    }

}
