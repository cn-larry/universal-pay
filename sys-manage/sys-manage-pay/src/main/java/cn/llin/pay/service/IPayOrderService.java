package cn.llin.pay.service;

import cn.llin.pay.domain.PayOrder;

import java.util.List;

/**
 * 交易记录Service接口
 *
 * @author larry
 * @date 2021-11-10
 */
public interface IPayOrderService {
    /**
     * 查询交易记录
     *
     * @param id 交易记录主键
     * @return 交易记录
     */
    public PayOrder selectPayOrderById(Long id);

    /**
     * 查询交易记录列表
     *
     * @param payOrder 交易记录
     * @return 交易记录集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增交易记录
     *
     * @param payOrder 交易记录
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改交易记录
     *
     * @param payOrder 交易记录
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 批量删除交易记录
     *
     * @param ids 需要删除的交易记录主键集合
     * @return 结果
     */
    public int deletePayOrderByIds(String ids);

    /**
     * 删除交易记录信息
     *
     * @param id 交易记录主键
     * @return 结果
     */
    public int deletePayOrderById(Long id);
}
