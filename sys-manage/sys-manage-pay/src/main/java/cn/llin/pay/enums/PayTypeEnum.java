package cn.llin.pay.enums;

/**
 * Larry
 * 2021/11/10 22:58
 * 交易类型
 *
 * @Version 1.0
 */
public enum PayTypeEnum {

    /**
     * 支付宝支付
     */
    ALIPAY,

    /**
     * 微信支付
     */
    WEIXIN,

    /**
     * 银联支付（云闪付或者其他的银行）
     */
    UNIONPAY


}
