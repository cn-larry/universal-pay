package cn.llin.pay.domain;

import cn.llin.common.annotation.Excel;
import cn.llin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商户交易参数对象 pay_mer_payparameter
 *
 * @author larry
 * @date 2021-11-10
 */
public class PayMerPayparameter extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 商户ID
     */
    private Long merId;

    /**
     * 支付code
     */
    @Excel(name = "支付code" )
    private String payCode;

    /**
     * 支付参数
     */
    @Excel(name = "支付参数" )
    private String payParameter;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setMerId(Long merId) {
        this.merId = merId;
    }

    public Long getMerId() {
        return merId;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayParameter(String payParameter) {
        this.payParameter = payParameter;
    }

    public String getPayParameter() {
        return payParameter;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("merId" , getMerId())
                .append("payCode" , getPayCode())
                .append("payParameter" , getPayParameter())
                .append("delFlag" , getDelFlag())
                .append("createBy" , getCreateBy())
                .append("createTime" , getCreateTime())
                .append("updateBy" , getUpdateBy())
                .append("updateTime" , getUpdateTime())
                .append("remark" , getRemark())
                .toString();
    }
}
