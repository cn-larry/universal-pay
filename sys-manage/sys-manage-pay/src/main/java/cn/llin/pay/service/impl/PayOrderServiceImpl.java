package cn.llin.pay.service.impl;

import cn.llin.common.core.text.Convert;
import cn.llin.common.utils.DateUtils;
import cn.llin.common.utils.StringUtils;
import cn.llin.pay.domain.PayOrder;
import cn.llin.pay.domain.PayOrderRefund;
import cn.llin.pay.mapper.PayOrderMapper;
import cn.llin.pay.service.IPayOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 交易记录Service业务层处理
 *
 * @author larry
 * @date 2021-11-10
 */
@Service
public class PayOrderServiceImpl implements IPayOrderService {

    @Autowired
    private PayOrderMapper payOrderMapper;

    /**
     * 查询交易记录
     *
     * @param id 交易记录主键
     * @return 交易记录
     */
    @Override
    public PayOrder selectPayOrderById(Long id) {
        return payOrderMapper.selectPayOrderById(id);
    }

    /**
     * 查询交易记录列表
     *
     * @param payOrder 交易记录
     * @return 交易记录
     */
    @Override
    public List<PayOrder> selectPayOrderList(PayOrder payOrder) {
        return payOrderMapper.selectPayOrderList(payOrder);
    }

    /**
     * 新增交易记录
     *
     * @param payOrder 交易记录
     * @return 结果
     */
    @Transactional
    @Override
    public int insertPayOrder(PayOrder payOrder) {
        payOrder.setCreateTime(DateUtils.getNowDate());
        int rows = payOrderMapper.insertPayOrder(payOrder);
        insertPayOrderRefund(payOrder);
        return rows;
    }

    /**
     * 修改交易记录
     *
     * @param payOrder 交易记录
     * @return 结果
     */
    @Transactional
    @Override
    public int updatePayOrder(PayOrder payOrder) {
        payOrder.setUpdateTime(DateUtils.getNowDate());
        payOrderMapper.deletePayOrderRefundByPayNo(payOrder.getId());
        insertPayOrderRefund(payOrder);
        return payOrderMapper.updatePayOrder(payOrder);
    }

    /**
     * 批量删除交易记录
     *
     * @param ids 需要删除的交易记录主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePayOrderByIds(String ids) {
        payOrderMapper.deletePayOrderRefundByPayNos(Convert.toStrArray(ids));
        return payOrderMapper.deletePayOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除交易记录信息
     *
     * @param id 交易记录主键
     * @return 结果
     */
    @Override
    public int deletePayOrderById(Long id) {
        payOrderMapper.deletePayOrderRefundByPayNo(id);
        return payOrderMapper.deletePayOrderById(id);
    }

    /**
     * 新增退款信息
     *
     * @param payOrder 交易记录对象
     */
    public void insertPayOrderRefund(PayOrder payOrder) {
        List<PayOrderRefund> payOrderRefundList = payOrder.getPayOrderRefundList();
        String payNo = payOrder.getPayNo();
        if (StringUtils.isNotNull(payOrderRefundList)) {
            List<PayOrderRefund> list = new ArrayList<PayOrderRefund>();
            for (PayOrderRefund payOrderRefund : payOrderRefundList) {
                payOrderRefund.setPayNo(payNo);
                list.add(payOrderRefund);
            }
            if (list.size() > 0) {
                payOrderMapper.batchPayOrderRefund(list);
            }
        }
    }
}
