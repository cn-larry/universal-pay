package cn.llin.pay.domain;

import java.util.List;

import cn.llin.common.annotation.Excel;
import cn.llin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 退款对象 pay_order_refund
 *
 * @author Larry
 * @date 2021-11-10
 */
public class PayOrderRefund extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 退款订单号
     */
    @Excel(name = "退款订单号" )
    private String refundNo;

    /**
     * 原交易系统订单号
     */
    @Excel(name = "原交易系统订单号" )
    private String payNo;

    /**
     * 退款状态
     */
    @Excel(name = "退款状态" )
    private String refundStatus;

    /**
     * 退款金额
     */
    @Excel(name = "退款金额（单位：分）" )
    private Long refundAmt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    public String getPayNo() {
        return payNo;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundAmt(Long refundAmt) {
        this.refundAmt = refundAmt;
    }

    public Long getRefundAmt() {
        return refundAmt;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id" , getId())
                .append("refundNo" , getRefundNo())
                .append("payNo" , getPayNo())
                .append("refundStatus" , getRefundStatus())
                .append("refundAmt" , getRefundAmt())
                .append("remark" , getRemark())
                .append("delFlag" , getDelFlag())
                .append("createTime" , getCreateTime())
                .append("updateTime" , getUpdateTime())
                .toString();
    }
}
