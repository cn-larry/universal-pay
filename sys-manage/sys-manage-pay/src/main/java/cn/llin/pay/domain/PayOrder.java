package cn.llin.pay.domain;

import java.util.List;
import java.util.Date;

import cn.llin.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.llin.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 交易记录对象 pay_order
 *
 * @author larry
 * @date 2021-11-10
 */
public class PayOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 订单时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @Excel(name = "订单时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss" )
    private Date orderTime;

    /**
     * 订单支付时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @Excel(name = "订单支付时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss" )
    private Date payTime;

    /**
     * 交易类型
     */
    @Excel(name = "交易类型" )
    private String payType;

    /**
     * 系统交易订单号
     */
    @Excel(name = "系统交易订单号" )
    private String payNo;

    /**
     * 渠道商户交易单号
     */
    @Excel(name = "渠道商户交易单号" )
    private String channelMerNo;

    /**
     * 渠道支付单号
     */
    @Excel(name = "渠道支付单号" )
    private String channelPayNo;

    /**
     * 消费者用户ID
     */
    @Excel(name = "消费者用户ID" )
    private String payUserId;

    /**
     * 商户ID
     */
    @Excel(name = "商户ID" )
    private Long merId;

    /**
     * 收款码ID
     */
    @Excel(name = "收款码ID" )
    private String qrcodeId;

    /**
     * 交易状态
     */
    @Excel(name = "交易状态" )
    private String payStatus;

    /**
     * 订单金额（单位：分）
     */
    @Excel(name = "订单金额（单位：分）")
    private Long payAmt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 退款信息
     */
    private List<PayOrderRefund> payOrderRefundList;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    public String getPayNo() {
        return payNo;
    }

    public void setChannelMerNo(String channelMerNo) {
        this.channelMerNo = channelMerNo;
    }

    public String getChannelMerNo() {
        return channelMerNo;
    }

    public void setChannelPayNo(String channelPayNo) {
        this.channelPayNo = channelPayNo;
    }

    public String getChannelPayNo() {
        return channelPayNo;
    }

    public void setPayUserId(String payUserId) {
        this.payUserId = payUserId;
    }

    public String getPayUserId() {
        return payUserId;
    }

    public void setMerId(Long merId) {
        this.merId = merId;
    }

    public Long getMerId() {
        return merId;
    }

    public void setQrcodeId(String qrcodeId) {
        this.qrcodeId = qrcodeId;
    }

    public String getQrcodeId() {
        return qrcodeId;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayAmt(Long payAmt) {
        this.payAmt = payAmt;
    }

    public Long getPayAmt() {
        return payAmt;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public List<PayOrderRefund> getPayOrderRefundList() {
        return payOrderRefundList;
    }

    public void setPayOrderRefundList(List<PayOrderRefund> payOrderRefundList) {
        this.payOrderRefundList = payOrderRefundList;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id" , getId())
                .append("orderTime" , getOrderTime())
                .append("payTime" , getPayTime())
                .append("payType" , getPayType())
                .append("payNo" , getPayNo())
                .append("channelMerNo" , getChannelMerNo())
                .append("channelPayNo" , getChannelPayNo())
                .append("payUserId" , getPayUserId())
                .append("merId" , getMerId())
                .append("qrcodeId" , getQrcodeId())
                .append("payStatus" , getPayStatus())
                .append("payAmt" , getPayAmt())
                .append("remark" , getRemark())
                .append("delFlag" , getDelFlag())
                .append("createTime" , getCreateTime())
                .append("updateTime" , getUpdateTime())
                .append("payOrderRefundList" , getPayOrderRefundList())
                .toString();
    }
}
