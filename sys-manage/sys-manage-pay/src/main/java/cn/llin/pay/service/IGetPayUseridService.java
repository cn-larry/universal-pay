package cn.llin.pay.service;

import cn.llin.pay.exceptions.SysException;

/**
 * 获取消费者用户标识Service接口
 *
 * @author larry
 * @date 2021-11-10
 */
public interface IGetPayUseridService {

    /**
     * 根据二维码ID拼接请求微信URL（获取用户授权Code的URL）
     *
     * @param qrCodeId
     * @return
     * @throws Exception
     */
    public String getReqWeixinUrl(String qrCodeId) throws SysException;

    /**
     * 使用微信用户临时授权的code获取用户openid
     *
     * @param code
     * @return
     * @throws Exception
     */
    public String getWeinxinOpenid(String code) throws SysException;

}
