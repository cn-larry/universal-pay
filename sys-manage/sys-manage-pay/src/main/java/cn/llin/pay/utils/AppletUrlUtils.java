package cn.llin.pay.utils;

import cn.com.llin.string.StringTool;
import cn.com.llin.weixin.WeixinUtils;
import cn.llin.pay.exceptions.SysException;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayOpenAppQrcodeCreateRequest;
import com.alipay.api.response.AlipayOpenAppQrcodeCreateResponse;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * Larry
 * 2021/11/17 09:59
 *
 * @Version 1.0
 */
public class AppletUrlUtils {

    /**
     * 获取微信小程序URL链接
     *
     * @param appid
     * @param appsecret
     * @param appletPayPath
     * @param pathQuery
     * @param redisTemplate
     * @param expireIntervalDay
     * @return
     * @throws SysException
     */
    public static String getWeixinAppletUrl(String appid,
                                            String appsecret,
                                            String appletPayPath,
                                            String pathQuery,
                                            RedisTemplate redisTemplate,
                                            int expireIntervalDay) throws SysException {

        Object tokenObject = redisTemplate.opsForValue().get(RedisUtils.REDIS_KEY_HEAD_SYS_MANAGE_WEIXIN_QPPID_TOKEN + appid);
        String token = tokenObject == null ? "" : tokenObject.toString();
        if (StringTool.isBlank(token)) {
            String respStr = WeixinUtils.getPublicWeixinToken(appid, appsecret);
            JSONObject jsonObject = JSONObject.parseObject(respStr);
            Object access_token = jsonObject.get("access_token");
            if (access_token == null)
                throw new SysException("request weixin is error");

            // 设置有效时间
            long expires_in = (long) jsonObject.get("expires_in") - 60000;
            redisTemplate.opsForValue().set(RedisUtils.REDIS_KEY_HEAD_SYS_MANAGE_WEIXIN_QPPID_TOKEN + appid,
                    access_token.toString(), expires_in, TimeUnit.SECONDS);
            token = access_token.toString();
        }

        try {
            return WeixinUtils.getAppletUrllink(token, appletPayPath, pathQuery, true, 1, expireIntervalDay);
        } catch (Exception e) {
            throw new SysException("get UrlLink is Exception");
        }
    }

    /**
     * 获取支付小程序URL链接
     * （参考：https://opendocs.alipay.com/mini/api/xqvxl4）
     *
     * @param appid
     * @param appPrivateKey
     * @param alipayPublicKey
     * @param appletPayPath
     * @param queryParam
     * @param describe
     * @return
     * @throws SysException
     */
    public static String getAlipayAppletUrl(String appid,
                                            String appPrivateKey,
                                            String alipayPublicKey,
                                            String appletPayPath,
                                            String queryParam,
                                            String describe) throws SysException {

        if (StringTool.isBlank(describe))
            describe = "支付宝小程序";

        try {
            AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
                    appid,
                    appPrivateKey,
                    "json",
                    "GBK",
                    alipayPublicKey,
                    "RSA2");
            AlipayOpenAppQrcodeCreateRequest request = new AlipayOpenAppQrcodeCreateRequest();
            request.setBizContent("{" +
                    "\"url_param\":\"" + appletPayPath + "\"," +
                    "\"query_param\":\"" + queryParam + "\"," +
                    "\"describe\":\"" + describe + "\"" +
                    "  }");
            AlipayOpenAppQrcodeCreateResponse response = alipayClient.execute(request);
            if (response.isSuccess())
                return response.getQrCodeUrl();
            else
                throw new SysException("Get AlipayApplet Url Fail");
        } catch (Exception e) {
            throw new SysException("Get AlipayApplet Url Exception");
        }
    }

}
