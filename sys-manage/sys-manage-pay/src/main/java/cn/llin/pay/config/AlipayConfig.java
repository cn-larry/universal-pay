package cn.llin.pay.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Larry
 * 2021/11/16 21:31
 * 支付宝相关配置
 *
 * @Version 1.0
 */
@Data
@Configuration
public class AlipayConfig {

    /**
     * 支付宝应用APPID
     */
    @Value("${alipay.appid}" )
    private String appid;

    /**
     * 支付宝应用密钥
     */
    @Value("${alipay.appPrivateKey}" )
    private String appPrivateKey;

    /**
     * 支付宝公钥
     */
    @Value("${alipay.alipayPublicKey}" )
    private String alipayPublicKey;

    /**
     * 小程序付款页面
     */
    @Value("${alipay.appletPayPath}" )
    private String appletPayPath;

}
