package cn.llin.pay.mapper;

import cn.llin.pay.domain.PayMer;
import cn.llin.pay.domain.PayMerPayparameter;

import java.util.List;

/**
 * 商户Mapper接口
 *
 * @author larry
 * @date 2021-11-10
 */
public interface PayMerMapper {
    /**
     * 查询商户
     *
     * @param merId 商户主键
     * @return 商户
     */
    public PayMer selectPayMerByMerId(Long merId);

    /**
     * 查询商户列表
     *
     * @param payMer 商户
     * @return 商户集合
     */
    public List<PayMer> selectPayMerList(PayMer payMer);

    /**
     * 新增商户
     *
     * @param payMer 商户
     * @return 结果
     */
    public int insertPayMer(PayMer payMer);

    /**
     * 修改商户
     *
     * @param payMer 商户
     * @return 结果
     */
    public int updatePayMer(PayMer payMer);

    /**
     * 删除商户
     *
     * @param merId 商户主键
     * @return 结果
     */
    public int deletePayMerByMerId(Long merId);

    /**
     * 批量删除商户
     *
     * @param merIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayMerByMerIds(String[] merIds);

    /**
     * 批量删除商户交易参数
     *
     * @param merIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayMerPayparameterByMerIds(String[] merIds);

    /**
     * 批量新增商户交易参数
     *
     * @param payMerPayparameterList 商户交易参数列表
     * @return 结果
     */
    public int batchPayMerPayparameter(List<PayMerPayparameter> payMerPayparameterList);


    /**
     * 通过商户主键删除商户交易参数信息
     *
     * @param merId 商户ID
     * @return 结果
     */
    public int deletePayMerPayparameterByMerId(Long merId);
}
