package cn.llin.pay.enums;

/**
 * Larry
 * 2021/11/10 10:58
 * 商户状态
 *
 * @Version 1.0
 */
public enum MerStateEnum {

    /**
     * 商户资料审核中
     */
    UNDER_REVIEW,

    /**
     * 商户资料审核通过（可用的状态）
     */
    APPROVED,

    /**
     * 商家营业中
     */
    NORMAL_BUSINESS,

    /**
     * 商家休息
     */
    SHOP_REST,

    /**
     * 商家已关闭（不营业）
     */
    STORE_CLOSED

}
