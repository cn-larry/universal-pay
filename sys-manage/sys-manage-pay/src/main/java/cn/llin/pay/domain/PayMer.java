package cn.llin.pay.domain;

import java.util.List;

import cn.llin.common.annotation.Excel;
import cn.llin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商户对象 pay_mer
 *
 * @author larry
 * @date 2021-11-10
 */
public class PayMer extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 商户ID
     */
    private Long merId;

    /**
     * 商户名称
     */
    @Excel(name = "商户名称" )
    private String merName;

    /**
     * 商户简称
     */
    @Excel(name = "商户简称" )
    private String merSubName;

    /**
     * 商户状态（0正常 1停用）
     */
    @Excel(name = "商户状态" , readConverterExp = "0=正常,1=停用" )
    private String merStatus;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 商户交易参数信息
     */
    private List<PayMerPayparameter> payMerPayparameterList;

    public void setMerId(Long merId) {
        this.merId = merId;
    }

    public Long getMerId() {
        return merId;
    }

    public void setMerName(String merName) {
        this.merName = merName;
    }

    public String getMerName() {
        return merName;
    }

    public void setMerSubName(String merSubName) {
        this.merSubName = merSubName;
    }

    public String getMerSubName() {
        return merSubName;
    }

    public void setMerStatus(String merStatus) {
        this.merStatus = merStatus;
    }

    public String getMerStatus() {
        return merStatus;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public List<PayMerPayparameter> getPayMerPayparameterList() {
        return payMerPayparameterList;
    }

    public void setPayMerPayparameterList(List<PayMerPayparameter> payMerPayparameterList) {
        this.payMerPayparameterList = payMerPayparameterList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("merId" , getMerId())
                .append("merName" , getMerName())
                .append("merSubName" , getMerSubName())
                .append("merStatus" , getMerStatus())
                .append("delFlag" , getDelFlag())
                .append("createBy" , getCreateBy())
                .append("createTime" , getCreateTime())
                .append("updateBy" , getUpdateBy())
                .append("updateTime" , getUpdateTime())
                .append("remark" , getRemark())
                .append("payMerPayparameterList" , getPayMerPayparameterList())
                .toString();
    }
}
