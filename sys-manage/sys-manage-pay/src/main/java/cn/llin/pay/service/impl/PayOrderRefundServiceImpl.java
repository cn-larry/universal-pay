package cn.llin.pay.service.impl;

import cn.llin.common.core.text.Convert;
import cn.llin.common.utils.DateUtils;
import cn.llin.pay.domain.PayOrderRefund;
import cn.llin.pay.mapper.PayOrderRefundMapper;
import cn.llin.pay.service.IPayOrderRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 退款Service业务层处理
 *
 * @author Larry
 * @date 2021-11-10
 */
@Service
public class PayOrderRefundServiceImpl implements IPayOrderRefundService {

    @Autowired
    private PayOrderRefundMapper payOrderRefundMapper;

    /**
     * 查询退款
     *
     * @param id 退款主键
     * @return 退款
     */
    @Override
    public PayOrderRefund selectPayOrderRefundById(Long id) {
        return payOrderRefundMapper.selectPayOrderRefundById(id);
    }

    /**
     * 查询退款列表
     *
     * @param payOrderRefund 退款
     * @return 退款
     */
    @Override
    public List<PayOrderRefund> selectPayOrderRefundList(PayOrderRefund payOrderRefund) {
        return payOrderRefundMapper.selectPayOrderRefundList(payOrderRefund);
    }

    /**
     * 新增退款
     *
     * @param payOrderRefund 退款
     * @return 结果
     */
    @Override
    public int insertPayOrderRefund(PayOrderRefund payOrderRefund) {
        payOrderRefund.setCreateTime(DateUtils.getNowDate());
        return payOrderRefundMapper.insertPayOrderRefund(payOrderRefund);
    }

    /**
     * 修改退款
     *
     * @param payOrderRefund 退款
     * @return 结果
     */
    @Override
    public int updatePayOrderRefund(PayOrderRefund payOrderRefund) {
        payOrderRefund.setUpdateTime(DateUtils.getNowDate());
        return payOrderRefundMapper.updatePayOrderRefund(payOrderRefund);
    }

    /**
     * 批量删除退款
     *
     * @param ids 需要删除的退款主键
     * @return 结果
     */
    @Override
    public int deletePayOrderRefundByIds(String ids) {
        return payOrderRefundMapper.deletePayOrderRefundByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除退款信息
     *
     * @param id 退款主键
     * @return 结果
     */
    @Override
    public int deletePayOrderRefundById(Long id) {
        return payOrderRefundMapper.deletePayOrderRefundById(id);
    }
}
