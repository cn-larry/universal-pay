package cn.llin.pay.beans;

import cn.llin.pay.enums.PayTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Larry
 * 2021/11/10 20:25
 *
 * @Version 1.0
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QrCodeBean implements Serializable {

    /**
     * 收款二维码标识
     */
    private String qrcodeId;

    /**
     * 消费者临时授权标识
     */
    private String code;

    /**
     * 消费者标识
     */
    private String payUserId;

    /**
     * 交易类型
     */
    private PayTypeEnum payType;

}
