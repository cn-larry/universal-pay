package cn.llin.pay.beans;

import cn.llin.pay.domain.PayMer;
import cn.llin.pay.enums.PayTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

/**
 * Larry
 * 2021/11/10 20:25
 *
 * @Version 1.0
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QrCodePayQueryBean {

    private PayTypeEnum payType;

    private String payNo;

    private Long merId;
    private String qrcodeId;

    private String remark;
    private Long payAmt;

    private String payStatus;

    private Date orderTime;
    private Date payTime;
    private String channelMerNo;
    private String channelPayNo;
    private String payUserId;

    private PayMer payMer;

}
