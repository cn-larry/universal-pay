package cn.llin.pay.service;

import cn.llin.pay.domain.PayOrderRefund;

import java.util.List;

/**
 * 退款Service接口
 * 
 * @author Larry
 * @date 2021-11-10
 */
public interface IPayOrderRefundService {
    /**
     * 查询退款
     * 
     * @param id 退款主键
     * @return 退款
     */
    public PayOrderRefund selectPayOrderRefundById(Long id);

    /**
     * 查询退款列表
     * 
     * @param payOrderRefund 退款
     * @return 退款集合
     */
    public List<PayOrderRefund> selectPayOrderRefundList(PayOrderRefund payOrderRefund);

    /**
     * 新增退款
     * 
     * @param payOrderRefund 退款
     * @return 结果
     */
    public int insertPayOrderRefund(PayOrderRefund payOrderRefund);

    /**
     * 修改退款
     * 
     * @param payOrderRefund 退款
     * @return 结果
     */
    public int updatePayOrderRefund(PayOrderRefund payOrderRefund);

    /**
     * 批量删除退款
     * 
     * @param ids 需要删除的退款主键集合
     * @return 结果
     */
    public int deletePayOrderRefundByIds(String ids);

    /**
     * 删除退款信息
     * 
     * @param id 退款主键
     * @return 结果
     */
    public int deletePayOrderRefundById(Long id);
}
