package cn.llin.pay.service;

import cn.llin.pay.beans.QrCodePayCreateBean;
import cn.llin.pay.beans.QrCodePayNotifyBean;
import cn.llin.pay.beans.QrCodePayQueryBean;
import cn.llin.pay.enums.PayTypeEnum;
import cn.llin.pay.exceptions.SysException;

/**
 * 二维码交易相关Service接口
 *
 * @author larry
 * @date 2021-11-10
 */
public interface IQrCodePayService {


    /**
     * 创建交易链接（适用于外部订单创建后跳转至小程序内付款）
     *
     * @param bean
     * @return
     * @throws SysException
     */
    public String createPayUrl(QrCodePayCreateBean bean) throws SysException;


    /**
     * 交易创建（适用于'创建交易链接'后请求）
     *
     * @param payNo
     * @return
     * @throws SysException
     */
    public QrCodePayCreateBean create(String payNo) throws SysException;

    /**
     * （收款二维码）交易创建
     *
     * @param bean
     * @return
     * @throws SysException
     */
    public QrCodePayCreateBean create(QrCodePayCreateBean bean) throws SysException;

    /**
     * 交易查询
     *
     * @param bean
     * @return
     * @throws SysException
     */
    public QrCodePayQueryBean query(QrCodePayQueryBean bean) throws SysException;

    /**
     * 交易成功（异步）通知
     *
     * @param bean
     * @return
     * @throws SysException
     */
    public Boolean successNotify(QrCodePayNotifyBean bean) throws SysException;

    /**
     * 获取微信小程序UrlLink
     *
     * @param merId
     * @param expireIntervalDay 有效天数
     * @return
     * @throws SysException
     */
    public String getWeixinAppletPayUrl(String merId, int expireIntervalDay) throws SysException;

    /**
     * 获取支付宝小程序二维码
     *
     * @param merId
     * @return
     * @throws SysException
     */
    public String getAlipayAppletUrl(String merId) throws SysException;


    /**
     * 获取小程序链接
     *
     * @param payType
     * @param merId
     * @param expireIntervalDay 只针对微信小程序有效（建议不超过10天）
     * @param queryParam
     * @return
     * @throws SysException
     */
    public String getAppletUrl(PayTypeEnum payType, String merId, int expireIntervalDay, String queryParam) throws SysException;

}
