package cn.llin.pay.domain;

import cn.llin.common.annotation.Excel;
import cn.llin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商户静态收款码对象 pay_mer_static_qrcode
 *
 * @author larry
 * @date 2021-11-10
 */
public class PayMerStaticQrcode extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 收款码ID
     */
    private String qrcodeId;

    /**
     * 收款码状态（1正常 0停用）
     */
    @Excel(name = "收款码状态" , readConverterExp = "1=正常,0=停用" )
    private String qrcodeStatus;

    /**
     * 商户ID
     */
    @Excel(name = "商户ID" )
    private Long merId;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setQrcodeId(String qrcodeId) {
        this.qrcodeId = qrcodeId;
    }

    public String getQrcodeId() {
        return qrcodeId;
    }

    public void setQrcodeStatus(String qrcodeStatus) {
        this.qrcodeStatus = qrcodeStatus;
    }

    public String getQrcodeStatus() {
        return qrcodeStatus;
    }

    public void setMerId(Long merId) {
        this.merId = merId;
    }

    public Long getMerId() {
        return merId;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("qrcodeId" , getQrcodeId())
                .append("qrcodeStatus" , getQrcodeStatus())
                .append("merId" , getMerId())
                .append("delFlag" , getDelFlag())
                .append("createBy" , getCreateBy())
                .append("createTime" , getCreateTime())
                .append("updateBy" , getUpdateBy())
                .append("updateTime" , getUpdateTime())
                .append("remark" , getRemark())
                .toString();
    }
}
