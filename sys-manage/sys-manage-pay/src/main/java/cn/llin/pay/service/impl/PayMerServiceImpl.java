package cn.llin.pay.service.impl;

import cn.llin.common.core.text.Convert;
import cn.llin.common.utils.DateUtils;
import cn.llin.common.utils.StringUtils;
import cn.llin.pay.domain.PayMer;
import cn.llin.pay.domain.PayMerPayparameter;
import cn.llin.pay.mapper.PayMerMapper;
import cn.llin.pay.service.IPayMerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 商户Service业务层处理
 *
 * @author larry
 * @date 2021-11-10
 */
@Service
public class PayMerServiceImpl implements IPayMerService {

    @Autowired
    private PayMerMapper payMerMapper;

    /**
     * 查询商户
     *
     * @param merId 商户主键
     * @return 商户
     */
    @Override
    public PayMer selectPayMerByMerId(Long merId) {
        return payMerMapper.selectPayMerByMerId(merId);
    }

    /**
     * 查询商户列表
     *
     * @param payMer 商户
     * @return 商户
     */
    @Override
    public List<PayMer> selectPayMerList(PayMer payMer) {
        return payMerMapper.selectPayMerList(payMer);
    }

    /**
     * 新增商户
     *
     * @param payMer 商户
     * @return 结果
     */
    @Transactional
    @Override
    public int insertPayMer(PayMer payMer) {
        payMer.setCreateTime(DateUtils.getNowDate());
        int rows = payMerMapper.insertPayMer(payMer);
        insertPayMerPayparameter(payMer);
        return rows;
    }

    /**
     * 修改商户
     *
     * @param payMer 商户
     * @return 结果
     */
    @Transactional
    @Override
    public int updatePayMer(PayMer payMer) {
        payMer.setUpdateTime(DateUtils.getNowDate());
        payMerMapper.deletePayMerPayparameterByMerId(payMer.getMerId());
        insertPayMerPayparameter(payMer);
        return payMerMapper.updatePayMer(payMer);
    }

    /**
     * 批量删除商户
     *
     * @param merIds 需要删除的商户主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePayMerByMerIds(String merIds) {
        payMerMapper.deletePayMerPayparameterByMerIds(Convert.toStrArray(merIds));
        return payMerMapper.deletePayMerByMerIds(Convert.toStrArray(merIds));
    }

    /**
     * 删除商户信息
     *
     * @param merId 商户主键
     * @return 结果
     */
    @Override
    public int deletePayMerByMerId(Long merId) {
        payMerMapper.deletePayMerPayparameterByMerId(merId);
        return payMerMapper.deletePayMerByMerId(merId);
    }

    /**
     * 新增商户交易参数信息
     *
     * @param payMer 商户对象
     */
    public void insertPayMerPayparameter(PayMer payMer) {
        List<PayMerPayparameter> payMerPayparameterList = payMer.getPayMerPayparameterList();
        Long merId = payMer.getMerId();
        if (StringUtils.isNotNull(payMerPayparameterList)) {
            List<PayMerPayparameter> list = new ArrayList<PayMerPayparameter>();
            for (PayMerPayparameter payMerPayparameter : payMerPayparameterList) {
                payMerPayparameter.setMerId(merId);
                list.add(payMerPayparameter);
            }
            if (list.size() > 0) {
                payMerMapper.batchPayMerPayparameter(list);
            }
        }
    }
}
