package cn.llin.pay.service.impl;

import cn.llin.common.core.text.Convert;
import cn.llin.common.utils.DateUtils;
import cn.llin.pay.domain.PayMerStaticQrcode;
import cn.llin.pay.mapper.PayMerStaticQrcodeMapper;
import cn.llin.pay.service.IPayMerStaticQrcodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商户静态收款码Service业务层处理
 *
 * @author larry
 * @date 2021-11-10
 */
@Service
public class PayMerStaticQrcodeServiceImpl implements IPayMerStaticQrcodeService {

    @Autowired
    private PayMerStaticQrcodeMapper payMerStaticQrcodeMapper;

    /**
     * 查询商户静态收款码
     *
     * @param qrcodeId 商户静态收款码主键
     * @return 商户静态收款码
     */
    @Override
    public PayMerStaticQrcode selectPayMerStaticQrcodeByQrcodeId(String qrcodeId) {
        return payMerStaticQrcodeMapper.selectPayMerStaticQrcodeByQrcodeId(qrcodeId);
    }

    /**
     * 查询商户静态收款码列表
     *
     * @param payMerStaticQrcode 商户静态收款码
     * @return 商户静态收款码
     */
    @Override
    public List<PayMerStaticQrcode> selectPayMerStaticQrcodeList(PayMerStaticQrcode payMerStaticQrcode) {
        return payMerStaticQrcodeMapper.selectPayMerStaticQrcodeList(payMerStaticQrcode);
    }

    /**
     * 新增商户静态收款码
     *
     * @param payMerStaticQrcode 商户静态收款码
     * @return 结果
     */
    @Override
    public int insertPayMerStaticQrcode(PayMerStaticQrcode payMerStaticQrcode) {
        payMerStaticQrcode.setCreateTime(DateUtils.getNowDate());
        return payMerStaticQrcodeMapper.insertPayMerStaticQrcode(payMerStaticQrcode);
    }

    /**
     * 修改商户静态收款码
     *
     * @param payMerStaticQrcode 商户静态收款码
     * @return 结果
     */
    @Override
    public int updatePayMerStaticQrcode(PayMerStaticQrcode payMerStaticQrcode) {
        payMerStaticQrcode.setUpdateTime(DateUtils.getNowDate());
        return payMerStaticQrcodeMapper.updatePayMerStaticQrcode(payMerStaticQrcode);
    }

    /**
     * 批量删除商户静态收款码
     *
     * @param qrcodeIds 需要删除的商户静态收款码主键
     * @return 结果
     */
    @Override
    public int deletePayMerStaticQrcodeByQrcodeIds(String qrcodeIds) {
        return payMerStaticQrcodeMapper.deletePayMerStaticQrcodeByQrcodeIds(Convert.toStrArray(qrcodeIds));
    }

    /**
     * 删除商户静态收款码信息
     *
     * @param qrcodeId 商户静态收款码主键
     * @return 结果
     */
    @Override
    public int deletePayMerStaticQrcodeByQrcodeId(String qrcodeId) {
        return payMerStaticQrcodeMapper.deletePayMerStaticQrcodeByQrcodeId(qrcodeId);
    }

    /**
     * 获取临时的小程序链接
     *
     * @param qrcodeId
     * @return
     */
    @Override
    public String temporaryUrl(String qrcodeId) {
        PayMerStaticQrcode payMerStaticQrcode = selectPayMerStaticQrcodeByQrcodeId(qrcodeId);
        return null;
    }
}
