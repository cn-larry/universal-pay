package cn.llin.pay.beans;

import cn.llin.pay.enums.PayTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

/**
 * Larry
 * 2021/11/10 20:25
 *
 * @Version 1.0
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QrCodePayNotifyBean {

    private PayTypeEnum payType;

    private String payNo;

    private Long payAmt;

    private Date payTime;
    private String channelMerNo;
    private String channelPayNo;
    private String payUserId;

}
