package cn.llin.pay.beans;

import cn.llin.pay.domain.PayMer;
import cn.llin.pay.enums.PayTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Larry
 * 2021/11/10 20:25
 *
 * @Version 1.0
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QrCodePayCreateBean implements Serializable {

    private Long merId;
    private String qrcodeId;

    private String remark;
    private Long payAmt;

    private String payStatus;

    private PayTypeEnum payType;

    private Date orderTime;
    private Date payTime;
    private String payNo;
    private String channelMerNo;
    private String channelPayNo;
    private String payUserId;

    private PayMer payMer;


    // 预支付交易会话标识（交易创建后返回）
    private String prepayId;

}
