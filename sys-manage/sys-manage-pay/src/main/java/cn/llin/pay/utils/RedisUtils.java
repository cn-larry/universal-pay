package cn.llin.pay.utils;

import cn.com.llin.string.StringTool;
import cn.com.llin.time.TimeTool;
import cn.llin.pay.domain.PayOrder;
import cn.llin.pay.enums.PayTypeEnum;
import cn.llin.pay.exceptions.SysException;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Larry
 * 2021/11/10 21:43
 *
 * @Version 1.0
 */
@Slf4j
@Component
public class RedisUtils {

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 小程序支付，订单创建支付URL标识
     */
    public static final String REDIS_KEY_HEAD_SYS_MANAGE_PAYNO = "sys.manage.payNo.";
    public static final String REDIS_KEY_HEAD_SYS_MANAGE_WEIXIN_QPPID_TOKEN = "sys.manage.weixin.qppid.token.";


    /**
     * @param payNo
     * @param payOrder
     */
    public void setPayNo(String payNo, PayOrder payOrder) {
        // 存redis，并设置有效期
        redisTemplate.opsForValue().set(REDIS_KEY_HEAD_SYS_MANAGE_PAYNO + payNo, payOrder, 10, TimeUnit.MINUTES);
    }

    /**
     * @param payNo
     * @return
     * @throws SysException
     */
    public PayOrder getPayNo(String payNo) throws SysException {
        if(StringTool.isBlank(payNo))
            throw new SysException("pay order is invalid!");

        Object object = redisTemplate.opsForValue().get(REDIS_KEY_HEAD_SYS_MANAGE_PAYNO + payNo);
        if (object == null)
            throw new SysException("pay order is invalid or does not exist!");
        return (PayOrder) object;
    }


}
