package cn.llin.pay.mapper;

import cn.llin.pay.domain.PayOrder;
import cn.llin.pay.domain.PayOrderRefund;

import java.util.List;

/**
 * 交易记录Mapper接口
 *
 * @author larry
 * @date 2021-11-10
 */
public interface PayOrderMapper {
    /**
     * 查询交易记录
     *
     * @param id 交易记录主键
     * @return 交易记录
     */
    public PayOrder selectPayOrderById(Long id);

    /**
     * 查询交易记录
     *
     * @param payNo
     * @return
     */
    public PayOrder selectPayOrderByPayNo(String payNo);

    /**
     * 查询交易记录列表
     *
     * @param payOrder 交易记录
     * @return 交易记录集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增交易记录
     *
     * @param payOrder 交易记录
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改交易记录
     *
     * @param payOrder 交易记录
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * （交易状态'未待支付'时）修改交易记录
     * @param payOrder
     * @return
     */
    public int updatePayOrderAndPayStateIsNOT_PAY(PayOrder payOrder);

    /**
     * 删除交易记录
     *
     * @param id 交易记录主键
     * @return 结果
     */
    public int deletePayOrderById(Long id);

    /**
     * 批量删除交易记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayOrderByIds(String[] ids);

    /**
     * 批量删除退款
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayOrderRefundByPayNos(String[] ids);

    /**
     * 批量新增退款
     *
     * @param payOrderRefundList 退款列表
     * @return 结果
     */
    public int batchPayOrderRefund(List<PayOrderRefund> payOrderRefundList);


    /**
     * 通过交易记录主键删除退款信息
     *
     * @param id 交易记录ID
     * @return 结果
     */
    public int deletePayOrderRefundByPayNo(Long id);
}
