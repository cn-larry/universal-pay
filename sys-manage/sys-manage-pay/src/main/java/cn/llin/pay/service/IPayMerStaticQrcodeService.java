package cn.llin.pay.service;

import cn.llin.pay.domain.PayMerStaticQrcode;

import java.util.List;

/**
 * 商户静态收款码Service接口
 *
 * @author larry
 * @date 2021-11-10
 */
public interface IPayMerStaticQrcodeService {
    /**
     * 查询商户静态收款码
     *
     * @param qrcodeId 商户静态收款码主键
     * @return 商户静态收款码
     */
    public PayMerStaticQrcode selectPayMerStaticQrcodeByQrcodeId(String qrcodeId);

    /**
     * 查询商户静态收款码列表
     *
     * @param payMerStaticQrcode 商户静态收款码
     * @return 商户静态收款码集合
     */
    public List<PayMerStaticQrcode> selectPayMerStaticQrcodeList(PayMerStaticQrcode payMerStaticQrcode);

    /**
     * 新增商户静态收款码
     *
     * @param payMerStaticQrcode 商户静态收款码
     * @return 结果
     */
    public int insertPayMerStaticQrcode(PayMerStaticQrcode payMerStaticQrcode);

    /**
     * 修改商户静态收款码
     *
     * @param payMerStaticQrcode 商户静态收款码
     * @return 结果
     */
    public int updatePayMerStaticQrcode(PayMerStaticQrcode payMerStaticQrcode);

    /**
     * 批量删除商户静态收款码
     *
     * @param qrcodeIds 需要删除的商户静态收款码主键集合
     * @return 结果
     */
    public int deletePayMerStaticQrcodeByQrcodeIds(String qrcodeIds);

    /**
     * 删除商户静态收款码信息
     *
     * @param qrcodeId 商户静态收款码主键
     * @return 结果
     */
    public int deletePayMerStaticQrcodeByQrcodeId(String qrcodeId);

    /**
     * 获取临时的小程序链接
     *
     * @param qrcodeId
     * @return
     */
    public String temporaryUrl(String qrcodeId);

}
